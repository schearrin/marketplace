# **README** #

## **Collaborateurs** ##

Miary RALAMBOMAMY

Reda YACINI

Sophie CHEANG

Jean-Baptiste SCHMITT

## **Informations** ##

Projet XML - Réalisation d'un site Web d'une société de diffusion de vente à distance MarketPlace.

Professeur : Kim NGUYEN

## **Installation** ##

### Requis ###
* Installer java JRE ou JDK version 8u111 ("Java SE Development Kit 8u111") : 
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

* Installer PostgreSQL

### Utilisation de GIT et Eclipse ###

* Eviter les conflits quand on est sur la même branche : PULL avant de PUSH

* Configuration d'Eclipse pour la création du projet et l'import du projet :

**Le créateur du repositoire :**
Créer le projet : File > Other > Dynamic Web Project

**Les collaborateurs :**
Importer le projet : File > Import existing repository > Select root directory
S'il y a des problèmes et que le projet n'est pas partagé : Clic droit > Team > Share project > Use or create repository in parent folder of project

### Erreurs récurrentes ###

* Si erreur concernant les dossiers manquant, ajouter :

code/build/classes
code/src
code/WebContent/WEB-INF/lib

* Si erreur concernant le build path : JRE System Library :

Clic droit sur "JRE System Library ..." > Alternate JRE > Sélectionner la bonne version "jre..."

* Si erreur concernant le "Project Facet" :

Clic droit sur projet > Properties > dans le menu de gauche, sélectionner Project Facet > dans le ligne "Java", changer la version