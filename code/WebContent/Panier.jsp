<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="PS.svg" />
<link rel="stylesheet" href="style.css" />
<title>Panier</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour ${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${empty user}">
			<h1>Panier</h1>
			<p>Vous n'�tes pas connect�. Cr�er un compte ou connectez vous
				pour valider votre panier.</p>
		</c:when>
		<c:otherwise>
			<h1>Panier</h1>
			<c:choose>
				<c:when test="${not empty panier}">
					<p>Vous avez ${panier.cardinal} articles dans votre Panier</p>
					<table>
						<col width="200px" />
						<col width="200px" />
						<col width="200px" />
						<tr>
							<th>Produit</th>
							<th>Quantit�</th>
							<th>Prix TTC</th>
						</tr>
						<c:forEach var="p" items="${panier.panier}">
							<tr>
								<td>${p.ID_P.nom}</td>
								<td>${p.quantite}</td>
								<td>${p.prix} &euro;</td>
							</tr>

						</c:forEach>
						<tr><th>Somme</th><th>${panier.cardinal}</th><th>${panier.valeur}</th></tr>
					</table>
					
					<form action="panier" method="post">
					<input type="hidden" name="form" value="1" />
					<p>Votre total TTC est de <strong>${panier.valeur} &euro;.</strong></p>
					
					<p>Votre adresse de facturation est ${user.zip}</p>

					<input type="submit" class="bouton" value="Acheter"/>
					</form>
					
				</c:when>
				<c:otherwise>
					<p>Votre panier est vide</p>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>
</body>
</html>