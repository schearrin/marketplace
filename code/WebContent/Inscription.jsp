<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="PS.svg" />
<link rel="stylesheet"
	href="style.css" />
<title>Inscription</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour ${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	<h1>Inscription</h1>
	<c:choose>
		<c:when test="${not empty user}">
			<p>Bienvenue cher ${fn:escapeXml(user.prenom)}</p>
			<p>Nous vous invitons � consulter <a href="liste">la liste de nos produits</a>. </p>
		</c:when>
		<c:otherwise>
			<form action=inscription method=post>
				<table width=800px >
				<col width=200px>
				<col width=300px>
					<tr>
						<td>Identifiant (ou email)</td>
						<td><input type=text name=login value=Login class=cinx /></td>
					</tr>
					<tr>
						<td>Mot de passe</td>
						<td><input type=password name=password class=cinx
							value=password /></td>
					</tr>
					<tr>
						<td>Pr�nom</td>
						<td><input type=text name=prenom class=cinx value=Pr�nom /></td>
					</tr>
					<tr>
						<td>Nom</td>
						<td><input type=text name=nom class=cinx value=Nom /></td>
					</tr>
					<tr>
						<td>Code Postal</td>
						<td><input type=text name=zip class=cinx value=ZIP /></td>
					</tr>
				</table>
				<p>Ajouter une carte bancaire. Vos informations bancaires seront
					enregistr�es en clair dans notre base de donn�e.</p>
				<table width=800px >
				<col width=350px>
				<col width=100px>
				<col width=250px>
					<tr>
						<td><input name=numero class=cinx value=numero /></td>
						<td><select name=annee class=cinx>
								<c:forEach var="i" begin="2016" end="2030">
									<option value="${i}">${i}</option>
								</c:forEach>
						</select> <select name=mois class=cinx>
								<c:forEach var="i" begin="1" end="12">
									<option value="${i}">${i}</option>
								</c:forEach>
						</select></td>
						<td><input name=code class=cinx value=code /></td>
					</tr>
				</table>
				<input type=submit value=Valider class=bouton>
			</form>
		</c:otherwise>
	</c:choose>
</body>
</html>