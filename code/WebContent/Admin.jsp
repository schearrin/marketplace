<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="style.css" />
<link rel="shortcut icon" href="PS.svg" />
<title>Admin</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour
							${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${not empty user}">
			<h1>Admistration du site</h1>
			<p>Vous n'avez pas les privil�ges n�cessaires pour acc�der �
				cette partie du site</p>
		</c:when>
		<c:when test="${not empty admin }">
			<h1>Admistration du site</h1>
			<c:if test="${not empty connect}">
				<div id=popup>
					<p>Bonjour ${admin}</p>
				</div>
			</c:if>
			<h2>
				Bienvenue <strong>${admin}</strong>
			</h2>
			<p>
				<a class=bouton href=deco style="display: block; width: 200px">D�connexion</a>
			</p>
			<p>
				<a href=forme class=bouton style="display: block; width: 200px">Ajouter
						des produits</a>
			</p>
			<h2>Interface principale</h2>
			<form action=admin method=post>
				<table width=800px>
					<col width=100px>
					<col width=300px>
					<col width=300px>
					<tr>
						<th>Supprimer</th>
						<th>Produits</th>
						<th>Ajouter Cat�gorie</th>
					</tr>
					<c:forEach var="v" items="${produit}">
						<tr>
							<td><input type=checkbox value="${v.ID_P}" name=lproduit /></td>
							<td><a href="info?idp=${v.ID_P}">${v.nom}</a></td>
							<td><input type="text" name="categorie" class="cinx" /></td>
						</tr>
					</c:forEach>
					<tr>
						<th>Supprimer</th>
						<th>Client</th>
						<th>Ajouter Mot Cl�</th>
					</tr>
					<c:forEach var="u" items="${luser}">
						<tr>
							<td><input type=checkbox value="${u.ID_C}" name=luser /></td>
							<td><a href="client?idc=${u.ID_C}" >${u.login}</a></td>
							<td><input type="text" name="keyword" class="cinx" /></td>
						</tr>
					</c:forEach>
					
				</table>
				<input type=hidden value=1 name=form> <input type=submit
					value=Valider class=bouton>
			</form>
			<h2>Faire un audit du site</h2>
			<form action=admin method=post>
				<input type=hidden value=2 name=form> <input type=submit
					value=Audit class=bouton>
				<c:if test="${not empty audit}">
					<a href="${audit}" target="_blanck" >Rapport audit</a>
				</c:if>
			</form>
		</c:when>
		<c:otherwise>
			<h1>Admistration du site</h1>
			<p>Vous devez �tre administrateur pour acc�der � cette page</p>
			<form action=admin method=post>
				<input type=text name=login /> <input type=password name=password />
				<input type=hidden value=3 name=form> <input type=submit
					value=Valider class=bouton />
			</form>
		</c:otherwise>
	</c:choose>
</body>
</html>