<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" indent="yes" />

	<xsl:template match="/">
		<DOCUMENT>
			<xsl:apply-templates select="*" />
		</DOCUMENT>
	</xsl:template>
	<xsl:template match="node()">
		<table border="0" width="90%" color="black">
			<ELEMENT name="{name(.)}">

				<!-- Pour chaque element : -->
				<xsl:for-each select="./*">

					<!-- Recupère les entêtes du tableau -->
					<xsl:if test="position() = 1">
						<tr>
							<ATTRIBUTES>
								<th>
									<xsl:value-of select="name(.)" />
								</th>
							</ATTRIBUTES>
							<CHILDREN>
								<xsl:for-each select="./*">
									<th>
										<xsl:value-of select="name(.)" />
									</th>
								</xsl:for-each>
							</CHILDREN>
						</tr>
					</xsl:if>

					<!-- Récupère les valeurs du tableau -->
					<tr>
						<!-- Attributs -->
						<ATTRIBUTES count="{count(./@*)}">
							<xsl:for-each select="./@*">
								<td>
									<xsl:element name="{name(.)}">
										<xsl:value-of select="." />
									</xsl:element>
								</td>
							</xsl:for-each>
						</ATTRIBUTES>

						<!-- Elements -->
						<CHILDREN>
							<xsl:for-each select="./*">
								<td>
									<!-- Appel du template parcours_arbre -->
									<xsl:call-template name="parcours_arbre">
										<xsl:with-param name="noeud" select="." />
										<xsl:with-param name="pos" select="0" />
									</xsl:call-template>
								</td>
							</xsl:for-each>
						</CHILDREN>
					</tr>
				</xsl:for-each>
			</ELEMENT>
		</table>
	</xsl:template>

	<!-- Permet de faire un parcours recursif dans l'arbre -->
	<xsl:template name="parcours_arbre">
		<xsl:param name="noeud" select="." />
		<xsl:param name="pos" select="0" />
		<xsl:choose>
			<!-- S'il existe des fils en dessous du noeud alors : -->
			<xsl:when test="$noeud/*">
				<ul>
				<xsl:for-each select="$noeud/*">
						<xsl:call-template name="parcours_arbre">
							<xsl:with-param name="noeud" select="." />
							<xsl:with-param name="pos" select="$pos + 1" />
						</xsl:call-template>
				</xsl:for-each>
				</ul>
			</xsl:when>
			<!-- Si c'est une feuille alors : -->
			<xsl:otherwise>
				<xsl:choose>
					<!-- Image -->
					<xsl:when
						test="contains($noeud, '.jpg') or contains($noeud, '.png') or contains($noeud, '.bmp')">
							<xsl:element name="img">
								<xsl:attribute name="src">
									<xsl:value-of select="$noeud" />
								</xsl:attribute>
							</xsl:element>
					</xsl:when>
					<!-- Texte -->
					<xsl:otherwise>
						<xsl:if test="$pos >= 1">
							<li>
								<xsl:value-of select="$noeud" />
							</li>
						</xsl:if>
						<xsl:if test="$pos = 0">
							<xsl:value-of select="$noeud" />
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
