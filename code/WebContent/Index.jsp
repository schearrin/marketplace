<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="PS.svg" />
<link rel="stylesheet" href="style.css" />
<title>Market Place</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${form == 1 }">
			<%@ include file="connect_form.jsp"%>
		</c:when>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour ${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">Déconnexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	
	<h1>Market Place</h1>
	<c:choose>
		<c:when test="${not empty notconnect}">
			<c:if test="${notconnect == 1 }">
			<p>
				Login ou Password incorrect.
			</p>
			</c:if>
			<c:if test="${notconnect == 0 }">
				<div id=popup>
					<p>Content de vous revoir ${fn:escapeXml(user.prenom)}</p>
				</div>
			</c:if>
		</c:when>
	</c:choose>
	
</body>
</html>