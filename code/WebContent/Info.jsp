<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="style.css" />
<link rel="shortcut icon" href="PS.svg" />
<title>D�tails Produit</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour ${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	<c:choose>


		<c:when test="${not empty form}">
			<h1>
				Modifier les d�tails du Produit <strong>${prod.nom}</strong>
			</h1>
			<h2>Ajouter une image</h2>
			<form action="upload" method="post" enctype="multipart/form-data">
				<input type="text" name="image" class=cinx style="width: 200px;"
					value="Titre" /> <input type="hidden" name="idp"
					value="${prod.ID_P}" /> <input type="hidden" name="objet"
					value="PRODUIT" /> <input type="file" name="file" class="bouton" />
				<input type="submit" value="upload" class="bouton" />
			</form>
			<h2>Modifier</h2>
			<form action="info" method="post">
				<input type="hidden" name="idp" value="${prod.ID_P}" />
				<table>
					<tr>
						<td>Nom</td>
						<td><input type="text" name="name" class=cinx
							value="${prod.nom}" />
					</tr>
					<tr>
						<td>Prix</td>
						<td><input type="text" name="prix" class=cinx
							value="${prod.prix}" /></td>
					</tr>
					<tr>
						<td>Vendeur</td>
						<td><input type="text" name="vendeur" class=cinx
							value="${prod.vendeur}" /></td>
					</tr>
					<tr>
						<td>Categories</td>
						<td><select size=10 name=idc class=cinx multiple>
								<c:forEach var="k" items="${prod.categorie}">
									<option value="${k.ID_CAT}" selected>${k.name}</option>
								</c:forEach>
								<c:forEach var="l" items="${categories}">
									<option value="${l.ID_CAT}">${l.name}</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td>Mots cl�s</td>
						<td><select size=10 name=idk class=cinx multiple>
								<c:forEach var="k" items="${prod.keyword}">
									<option value="${k.ID_KW}" selected>${k.name}</option>
								</c:forEach>
								<c:forEach var="l" items="${keywords}">
									<option value="${l.ID_KW}">${l.name}</option>
								</c:forEach>
						</select></td>
					</tr>
				</table>
				<h2>Supprimer des images</h2>
				<ul>
					<c:forEach var="c" items="${prod.image}">
						<li><INPUT type="checkbox" name="supprimg" value="${c.ID_I}">${c.chemin}</li>
					</c:forEach>
				</ul>

				<input type="hidden" name="form" value="3" /> <input type="submit"
					value="Valider" class="bouton" />

			</form>
		</c:when>

		<c:when test="${empty prod}">
			<h1>Produit</h1>
			<a href=liste>Liste des produits</a>
		</c:when>
		<c:otherwise>

			<h1>${prod.nom}</h1>
			<c:if test="${not empty admin}">
				<form action=info method=post>
					<input type=hidden name=form value=1 /> <input type=hidden
						name=idp value="${prod.ID_P}" /> <input type=submit
						value=Modifier class=bouton>
				</form>
			</c:if>
			<h2>Produit</h2>
			<table width=850px>
				<col width=450px>
				<col width=200px>
				<col width=200px>

				<tr>
					<td><c:forEach var="i" items="${prod.image}">
							<a href="${i.chemin}" target=_blanck><img src="${i.chemin}"
								style="display: inline-block; width: 200px; margin: 0; padding: 0;"
								title="${i.title}"></a>
						</c:forEach></td>
					<td>
						<div class="rating">
							<c:forEach var="i" begin="1" end="${prod.notation}">
								<a href="#${i}">&#9733;</a>
							</c:forEach>
						</div>
						<ul>

							<li><strong><a href="info?idp=${prod.ID_P}">${prod.nom}</a></strong></li>
							<li>Prix : <strong>${prod.prix} &euro;</strong></li>
							<li>Vendu par <strong>${prod.vendeur}</strong></li>

							<li>Cat�gories
								<ul>
									<c:forEach var="k" items="${prod.categorie}">
										<li>${k.name}</li>
									</c:forEach>
								</ul>
							</li>
							<li>Mots Cl�s
								<ul>
									<c:forEach var="k" items="${prod.keyword}">
										<li>${k.name}</li>
									</c:forEach>
								</ul>
							</li>
						</ul>
					</td>
					<td>

						<form action="info" method="post">
							<p>
								<strong>Ajouter au panier</strong>
							</p>
							<p>
								Quantit� : <input type="number" name="howmuch" /> <input
									type="hidden" name="form" value="4"/ > <input
									type="hidden" name="idp" value="${prod.ID_P }" /> <input
									type="submit" class="bouton" value="Ajouter au Panier" />
						</form> <c:if test="${not empty panier }">
							<p>
								Votre panier contient <strong>${panier.cardinal}
									articles</strong>.
							</p>
							<p>
								Payer maintenant ? ${panier.valeur} &euro; <a href="panier">Votre
									Panier</a>
							</p>

						</c:if>

					</td>
				</tr>
			</table>

			<h2>Produits similaires</h2>
			<ul>
				<c:forEach var="p" items="${similar}">
					<li><strong><a href="info?idp=${p.ID_P}"
							title="${p.prix} &euro;">${p.nom }</a></strong></li>
				</c:forEach>
			</ul>

			<h2>Commentaires</h2>
			<c:choose>
				<c:when test="${(not empty user) and (not empty comment)}">
					<form action=info method=post>
						<input type=hidden name=idc value="${user.ID_C}" /> <input
							type=hidden name=idp value="${prod.ID_P}" /> <input type=hidden
							name=form value=2 />

						<table>
							<tr>
								<td rowspan="2"><textarea name=text rows=4 cols=50>Ajouter un commentaire</textarea></td>
								<td>Note</td>
							</tr>
							<tr>
								<td><input type="range" min="1" max="5" value="1" step="1"
									name="note" list="powers"> <datalist id="powers">
									<option value="1">
									<option value="2">
									<option value="3">
									<option value="4">
									<option value="5">
									</datalist></td>
							</tr>
						</table>
						<input type=submit class=bouton />
					</form>
				</c:when>
			</c:choose>
			<form action=info method=post>
			<input type=hidden name=idc value="${user.ID_C}" /> 
			<input type=hidden name=idp value="${prod.ID_P}" /> 
			<input type=hidden name=form value=5 />
			<table width=90%>

				<tr>
					<th>Username</th>
					<th>Note</th>
					<th>Texte du commentaire</th>
					<th>Date</th>
				</tr>
				<c:forEach var="k" items="${prod.commentaire}">
					<tr>
						<td>${k.username}</td>
						<td><div class="rating">
								<c:forEach var="i" begin="1" end="${k.note}">
									<a href="#${i}">&#9733;</a>
								</c:forEach>
							</div></td>
						<td>
							<c:choose>
								<c:when test="${k.idc == user.ID_C}">
									<textarea name=text rows=4 cols=50>${k.text}</textarea>
								</c:when>
								<c:otherwise>
								${k.text}
								</c:otherwise>
							</c:choose>
						</td>
						<td>
						<c:choose>
							<c:when test="${k.idc == user.ID_C}">
							<input type=submit class=bouton />
							</c:when>
							<c:otherwise>
							${k.date}
							</c:otherwise>
						</c:choose>
						</td>
				</c:forEach>
			</table>
			</form>
		</c:otherwise>
	</c:choose>
</body>
</html>