<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="style.css" />
<title>File Upload</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour
							${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">Déconnexion</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	
	<h1>Résultat du téléchargement</h1>
	
	<div id="result">
		<h2>${message}</h2>
		<c:choose>
		<c:when test="${origine == 'PRODUIT'}"><a href="info?idp=${id}" >PRODUIT</a></c:when>
		<c:when test="${origine == 'CLIENT'}"><a href="compte" >COMPTE CLIENT</a></c:when>
		<c:otherwise><p>Consulter le message d'erreur et contacter <a href="mailto:jschmittbuc@gmail.com">jschmittbuc@gmail.com</a></p></c:otherwise>
		</c:choose>
	</div>
</body>
</html>

