<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="style.css" />
<link rel="shortcut icon" href="PS.svg" />
<title>Liste des produits</title>
</head>
<body>

	<%@ include file="Menu.jsp"%>

	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour
							${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>

	

	<h1>Liste de tous les Produits</h1>
	<p>Il y a ${count} produits sur le site !</p>
	<c:if test="${not empty success}">
		
		<form action="${success}" method=get>
		<input type=submit value="Consulter le fichier" class="bouton" class=submit />
	</form>
	</c:if>
	<c:if test="${not empty search }">
		<div id="popup">
			<p>${search}</p>
		</div>
	</c:if>
	
	
	<table width=90%>
		<col width=100px>
		<col width=200px>
		<col width=100px>
		<col width=100px>
		<col width=50px>
		<tr>
			<th>Notation</th>
			<th>Titre</th>
			<th>Vendeur</th>
			<th>Prix</th>
			<th>Mots cl�s</th>
			<th>Cat�gories</th>
			<th>Images</th>
		</tr>
		<c:forEach var="v" items="${produit}">
			<tr>
				<td>
				<div class="rating"><c:forEach var="i" begin="1" end="${v.notation}"><a href="#${i}" >&#9733;</a></c:forEach></div>
				
				</td>
				<td><a href="info?idp=${v.ID_P}"> <strong>${v.nom}
					</strong></a></td>
				<td>${v.vendeur}</td>
				<td>${v.prix} &euro;</td>
				<td><ul>
						<c:forEach var="i" items="${v.keyword}">
							<li>${i.name}</li>
						</c:forEach>
					</ul></td>
				<td><ul>
						<c:forEach var="i" items="${v.categorie}">
							<li>${i.name}</li>
						</c:forEach>
					</ul></td>
				<td><c:forEach var="i" items="${v.image}">
						<img src="${i.chemin}"  title="${i.title}"
							style="display: inline-block; width: 200px;" />
					</c:forEach></td>
			</tr>
		</c:forEach>
	</table>
	<c:choose>
		<c:when test="${not empty admin}">
			<h2>Catalogue</h2>
			<form action=liste method=post>
				<input type=hidden name=form value=1> <input type=submit
					value=GeneratePDF class=bouton>
			</form>
			<h2>Exporter la base de donn�e</h2>
			<form action=liste method=post>
				<input type=hidden name=form value=2> <input type=submit
					value=GenerateSQL class=bouton>
			</form>
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test="${not empty message}">
			<div id="popup">
				<p>${message}</p>
			</div>
		</c:when>
	</c:choose>
</body>
</html>