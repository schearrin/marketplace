<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="PS.svg" />
<link rel="stylesheet"
	href="style.css" />
<title>Formulaire Produit</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour ${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${not empty user}">
			<p>Vous n'avez pas l'autorisation d'acc�der � cette page</p>
		</c:when>
		<c:when test="${not empty admin}">
			<h1>Ajouter un admin</h1>
			<form action=forme method=post>
				<table width=500px>
					<col width=100px>
					<col width=400px>
					<tr>
						<td>Nom</td>
						<td><INPUT type=text name=name class=cinx value=Nom /></td>
					</tr>
					<tr>
						<td>Prix (xx.yy &euro;)</td>
						<td><INPUT type=text name=prix class=cinx value=Prix /></td>
					</tr>
					
					<tr>
						<td>Vendeur</td>
						<td><input type=text name=vendeur class=cinx value=Vendeur /></td>
					</tr>
					<tr>
						<td>Categorie</td>
						<td><select size=10 name=idc class=cinx multiple>
								<c:forEach var="k" items="${categorie}">
									<OPTION value="${k.ID_CAT}">${k.name}</OPTION>
								</c:forEach>
						</select></td>

					</tr>
					<tr>
						<td>Mots cl�</td>
						<td><SELECT size=10 name=idk class=cinx multiple>
								<c:forEach var="k" items="${keyword}">
									<OPTION value="${k.ID_KW}">${k.name}</OPTION>
								</c:forEach>
						</SELECT></td>

					</tr>
				</table>
				<input type=submit value=Ajouter class=bouton>
			</form>
		</c:when>
		<c:otherwise>
			<p>Vous devez �tre administrateur pour acc�der � cette page</p>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${not empty prod}">
			<a href="info?idp=${prod.ID_P}" >
				Nouveau Produit ajout� : ${prod.nom}
			</a>
		</c:when>
	</c:choose>
</body>
</html>