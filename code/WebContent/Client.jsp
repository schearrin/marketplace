<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="shortcut icon" href="PS.svg" />
<link rel="stylesheet" href="style.css" />
<title>Compte</title>
</head>
<body>
	<%@ include file="Menu.jsp"%>
	<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour ${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
					<li><a href="panier">Panier</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${not empty erreur}">
		<h1>Param�tres de compte</h1>
			<p>Vous n'�tes pas connect�. Cr�er un compte ou connectez vous
				pour acc�der � cette page.</p>
		</c:when>
		<c:when test="${not empty admin}">
			<h1>Param�tres de compte</h1>

			<h2>Informations personnelles</h2>
			<p>Login : ${user.login}</p>
			<form action=client method=post>
				
				<input type=hidden name=form value=1 />
				<input type=hidden name=idc value="${user.ID_C}" />
				<table>
					<tr>
						<td>Mot de passe</td>
						<td><input type=password name=password class=cinx
							value=password /></td>
					</tr>
					<tr>
						<td>Pr�nom</td>
						<td><input type=text name=prenom class=cinx
							value="${user.prenom }" /></td>
					</tr>
					<tr>
						<td>Nom</td>
						<td><input type=text name=nom class=cinx value="${user.nom }" /></td>
					</tr>
					<tr>
						<td>Code Postal</td>
						<td><input type=text name=zip class=cinx value="${user.zip }" /></td>
					</tr>
				</table>
				<h2>Ajouter une carte bancaire.</h2>
				<table>
					<tr>
						<td><input name=numero class=cinx value=numero /></td>
						<td><select name=annee class=cinx>
								<c:forEach var="i" begin="2016" end="2030">
									<option value="${i}">${i}</option>
								</c:forEach>
						</select> <select name=mois class=cinx>
								<c:forEach var="i" begin="1" end="12">
									<option value="${i}">${i}</option>
								</c:forEach>
						</select></td>
						<td><input name=code class=cinx value=code /></td>
					</tr>
				</table>
				<h2>Supprimer une carte bancaire.</h2>
				<ul>
					<c:forEach var="c" items="${user.carte}">
						<li><INPUT type="checkbox" name="supprcb" value="${c.ID_CB}">${c.num}</li>
					</c:forEach>
				</ul>
				<h2>Supprimer des images</h2>
				<ul>
					<c:forEach var="c" items="${user.photo}">
						<li><INPUT type="checkbox" name="supprimg" value="${c.ID_I}">${c.chemin}</li>
					</c:forEach>
				</ul>
				<input type=submit value=Valider class=bouton>
			</form>
		</c:when>
		<c:otherwise>
			<p>Vous n'�tes pas autoris� � acc�der � cette page</p>
		</c:otherwise>
	</c:choose>
</body>
</html>