<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="style.css" />
<title>Liste des produits</title>
</head>
<body>

	<%@ include file="Menu.jsp"%>
	
		<c:choose>
		<c:when test="${not empty user }">
			<div id=cssmenudroit
				style="margin-left: 75%; float: right; width: 10%; position: fixed; display: block;">
				<ul>
					<li><a href="compte">Bonjour
							${fn:escapeXml(user.prenom)}</a></li>
					<li><a href="deco">D�connexion</a></li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<%@ include file="connexion.jsp"%>
		</c:otherwise>
	</c:choose>
	
		<h1>Liste de tous les Produits de la cat�gorie export�e</h1>

	<!-- Applique le style xsl au document xml -->

	<c:import url="http://localhost:8080/projet-marketplace/import.xml" var="texte_xml" />
	<c:import url="http://localhost:8080/projet-marketplace/style.xsl" var="style_xslt" />
	<x:transform xml="${texte_xml}" xslt="${style_xslt}" />

	<!-- Formulaire -->
	<h2>Importer un fichier XML</h2>
	<form action="import" method="post" enctype="multipart/form-data">
    <input type="file" name="file" />
    <input type="submit" />
	</form>
	
	<c:set var="not_xmlfile" value="${not_xmlfile}" />
	<c:if test="${! empty not_xmlfile}">
		<p><c:out value="${not_xmlfile}" /></p>
	</c:if>
	
</body>
</html>