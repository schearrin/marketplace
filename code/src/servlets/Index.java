/**
 * 
 */
package servlets;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.Client;
import objets.ConnexionDB;



/**
 * Servlet qui contr�le la page principale
 * 
 * @author Arutha
 */
public class Index extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2711230070261574046L;

	/**
	 * Methode pour mettre les mots de passe en md5. Je l'ai mise en protected
	 * static parce que j'en ai aussi besoin dans Compte.java
	 * 
	 * @param password
	 * @return Le md5 de password
	 */
	protected static String encode(String password) {
		byte[] uniqueKey = password.getBytes();
		byte[] hash = null;

		try {
			hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
		} catch (NoSuchAlgorithmException e) {
			throw new Error("No MD5 support in this VM.");
		}

		StringBuilder hashString = new StringBuilder();
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(hash[i]);
			if (hex.length() == 1) {
				hashString.append('0');
				hashString.append(hex.charAt(hex.length() - 1));
			} else
				hashString.append(hex.substring(hex.length() - 2));
		}
		return hashString.toString();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute("db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("count", db.getNombreProduit());
			request.setAttribute("produit", db.getAllProduit());
			LinkedList<Client> lc = (LinkedList<Client>) db.getAllClient();
			int i = lc.size();
			System.out.println(i);
			request.setAttribute("message", i);
			request.setAttribute("client", lc);
			this.getServletContext().getRequestDispatcher("/Index.jsp")
			.forward(request, response);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			new ServletException(e.fillInStackTrace());
		}
		
		
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute("db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("count", db.getNombreProduit());
			request.setAttribute("produit", db.getAllProduit());
			
			
			
			String login = request.getParameter("login");
			String pw = request.getParameter("pw");
			int connect = 0;
			try {
				connect = Integer.parseInt(request.getParameter("connect"));
			} catch (NumberFormatException e) {

			}
			if (connect == 1) {
				request.setAttribute("form", 1);
			} else if (login != null && pw != null) {
				Client user = null;
				try {
					user = db.getUser(login, encode(pw));
				} catch (SQLException e) {
					e.printStackTrace();
				}
				if (user == null) {
					request.setAttribute("notconnect", 1);
				} else {
					request.setAttribute("notconnect", 0);
					request.getSession().setAttribute("user", user);
				}
			}
			this.getServletContext().getRequestDispatcher("/Index.jsp")
					.forward(request, response);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			new ServletException(e.fillInStackTrace());
		}
		
	}
}
