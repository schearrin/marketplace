package servlets;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.ConnexionDB;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet to handle File upload request from Client
 * 
 * @author Arutha
 */
public class Upload extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6712889381284235900L;
	private final String UPLOAD_DIRECTORY = "H:\\IMG";

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
		ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
				"db");
		if (db == null) {
			
				db = new ConnexionDB();
			
			request.getSession().setAttribute("db", db);
		}
		
		/*--------------------------------------------------------*/
		int ID = 0;
		String ORIGINE = "INCONNU"; 
		String TITRE = "TITRE"; 
		
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);
				String name = "V.jpg";
				String fieldvalue;
				String namevalue; 
				for (FileItem item : multiparts) {
					if (!item.isFormField()) {
						name = new File(item.getName()).getName();
						item.write(new File(UPLOAD_DIRECTORY + File.separator
								+ name));
					} else {
						fieldvalue = item.getString();
						namevalue = item.getFieldName();
						if (namevalue.equals("image")){
							TITRE = fieldvalue;
							System.out.println("NAMEVALUE " + fieldvalue);
						}else if (fieldvalue.equals("PRODUIT")){
							ORIGINE = fieldvalue;
						} else if (fieldvalue.equals("CLIENT")) {
							ORIGINE = fieldvalue; 
						} else {
							try {
								ID = Integer.parseInt(fieldvalue);
							} catch (NumberFormatException e) {
							}
						}
						
						
					}
				}
				
				/* ------------------------------------------------ */
				
				if (ORIGINE.equals("CLIENT")){
					db.insertImageClient(TITRE, ID, "/image/" + name);
					request.getSession().setAttribute("user", db.getClient(ID));
				} else if (ORIGINE.equals("PRODUIT")) {
					db.insertImageProduit (TITRE, ID, "/image/" + name);
				}
				
				
				request.setAttribute("message", "T�l�chargement r�ussi");
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("message", "Echec : " + e.getMessage());
			}
		} else {
			request.setAttribute("message", "D�sol�, fonction non pr�vue");
		}
		request.setAttribute("id", ID);
		request.setAttribute("origine", ORIGINE);
		request.getRequestDispatcher("/result.jsp").forward(request,
				response);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
