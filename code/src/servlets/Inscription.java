/**
 * 
 */
package servlets;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.Client;
import objets.ConnexionDB;

/**
 * @author Arutha
 * 
 */
public class Inscription extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("count", db.getNombreProduit());
			request.setAttribute("produit", db.getAllProduit());

			this.getServletContext().getRequestDispatcher("/Inscription.jsp")
					.forward(request, response);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("count", db.getNombreProduit());
			request.setAttribute("produit", db.getAllProduit());

			/**
			 * Récupération de la variable post
			 */
			String login = request.getParameter("login");
			String password = Index.encode(request.getParameter("password"));
			String nom = request.getParameter("nom");
			String prenom = request.getParameter("prenom");
			String zip = request.getParameter("zip");
			String numero = request.getParameter("numero");
			String code = request.getParameter("code");

			int annee = 0;
			int mois = 0;
			try {
				annee = Integer.parseInt(request.getParameter("annee"));
				mois = Integer.parseInt(request.getParameter("mois"));
			} catch (NumberFormatException e) {
			}
			String expire = annee + "-" + mois;

			/* Nouveau Client */

			Client user = db.insertClient(login, password, prenom, nom, zip);
			user = db.insertCarteBancaire(user.getID_C(), numero, expire, code);

			request.getSession().setAttribute("user", user);
			this.getServletContext().getRequestDispatcher("/Inscription.jsp")
					.forward(request, response);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static final long serialVersionUID = 4156486583486663653L;
}
