package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet pour se deco du site
 * @author Arutha
 */
public class Deconnexion extends HttpServlet {
	private static final long serialVersionUID = -8077634043667784771L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			request.getSession().removeAttribute("user");
			request.getSession().removeAttribute("admin");
			this.getServletContext().getRequestDispatcher("/Deconnexion.jsp")
					.forward(request, response);	
	}
}
