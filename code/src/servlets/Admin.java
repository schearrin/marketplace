/**
 * 
 */
package servlets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.Achat;
import objets.Carte;
import objets.Categorie;
import objets.Client;
import objets.ConnexionDB;
import objets.Keyword;
import objets.Produit;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author Arutha
 * 
 */
public class Admin extends HttpServlet {

	private static final long serialVersionUID = 6129999721691578947L;

	/**
	 * 
	 * @param path
	 * @param db
	 */
	protected static void generateAuditPDF(String path, ConnexionDB db) {
		/**
		 * com.itextpdf.tex.Document
		 */
		Document document = new Document();
		File fichier = new File(path);
		fichier.delete();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(path, false));
			document.open();

			List<Client> clients = db.getAllClient();
			List<Produit> produits = db.getAllProduit();

			/*
			 * Les clients d'abord
			 */
			document.add(new Paragraph("Clients:"));
			for (Client client : clients) {
				PdfPTable table = new PdfPTable(2);
				table.setWidths(new int[] { 1, 4 });
				PdfPCell cell;
				cell = new PdfPCell(new Phrase(client.getLogin()));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setColspan(2);
				table.addCell(cell);
				if (client.getAchat() != null) {
					cell = new PdfPCell(new Phrase(client.getAchat().size()
							+ " achats"));
					cell.setColspan(2);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
				}
				List<Achat> achats = client.getAchat();
				cell = new PdfPCell(new Phrase("Achats:"));
				cell.setRowspan(achats.size());
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(cell);
				int count = 0;
				for (Achat achat : achats) {
					cell = new PdfPCell(new Phrase(achat.getDateAchat()
							.toString()
							+ " : "
							+ achat.getProduit()
							+ " "
							+ achat.getQuantite() + " unit�s"));
					cell.setIndent(10 * count++);
					table.addCell(cell);
				}
				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_RIGHT);
				table.addCell("Nom:");
				table.addCell(String.valueOf(client.getNom()));
				table.addCell("Pr�nom:");
				table.addCell(String.valueOf(client.getPrenom()));
				List<Carte> cartes = client.getCarte();
				cell = new PdfPCell(new Phrase("Cartes bancaires:"));
				cell.setRowspan(cartes.size());
				cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
				table.addCell(cell);
				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_CENTER);
				for (Carte carte : cartes) {
					table.addCell(carte.getNum());
				}
				document.add(table);
			}

			/*
			 * Les produits ensuite
			 */
			document.add(new Paragraph("Produits:"));
			for (Produit produit : produits) {
				PdfPTable table = new PdfPTable(2);
				table.setWidths(new int[] { 1, 4 });
				PdfPCell cell;
				cell = new PdfPCell(new Phrase(produit.getNom()));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setColspan(2);
				table.addCell(cell);
				if (produit.getPrix() != null) {
					cell = new PdfPCell(new Phrase("EUR " + produit.getPrix()));
					cell.setColspan(2);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table.addCell(cell);
				}
				List<Categorie> cat = produit.getCategorie();
				cell = new PdfPCell(new Phrase("Categorie:"));
				cell.setRowspan(cat.size());
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(cell);
				int count = 0;
				for (Categorie c : cat) {
					cell = new PdfPCell(new Phrase(c.getName()));
					cell.setIndent(10 * count++);
					table.addCell(cell);
				}
				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_RIGHT);
				table.addCell("Vendeur:");
				table.addCell(String.valueOf(produit.getVendeur()));
				table.addCell("Commentaires:");
				table.addCell(String.valueOf(produit.getCommentaire().size()
						+ " commentaires"));
				List<Keyword> kw = produit.getKeyword();
				cell = new PdfPCell(new Phrase("Mots cl�s:"));
				cell.setRowspan(kw.size());
				cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
				table.addCell(cell);
				table.getDefaultCell().setHorizontalAlignment(
						Element.ALIGN_CENTER);
				for (Keyword k : kw) {
					table.addCell(k.getName());
				}
				document.add(table);
			}

			document.add(new Paragraph("Chiffre d'Affaire : EUR "
					+ db.getCA().toString()));

			document.close();

		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("produit", db.getAllProduit());
			request.setAttribute("luser", db.getAllClient());


			this.getServletContext().getRequestDispatcher("/Admin.jsp")
					.forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("produit", db.getAllProduit());
			request.setAttribute("luser", db.getAllClient());

			

			/*-----------------*/
			int form = 0;
			try {
				form = Integer.parseInt(request.getParameter("form"));
			} catch (NumberFormatException e) {
			}
			if (form == 1) {
				/* Liste des produits � supprimer */
				List<Integer> lproduit = new LinkedList<Integer>();
				String[] ls1 = request.getParameterValues("lproduit");
				if (ls1 == null) {
				} else {
					for (String s : ls1) {
						lproduit.add(Integer.parseInt(s));
					}
				}
				if (!lproduit.isEmpty()) {
					db.deleteProduits(lproduit);
				}

				/* Liste des clients � supprimer */
				List<Integer> luser = new LinkedList<Integer>();
				String[] ls2 = request.getParameterValues("luser");
				if (ls2 == null) {
				} else {
					for (String s : ls2) {
						luser.add(Integer.parseInt(s));
					}
				}
				if (!luser.isEmpty()) {
					db.deleteUsers(luser);
				}

				/* Liste des cat�gories � ajouter */
				List<String> categorie = new LinkedList<String>();
				String[] lc = request.getParameterValues("categorie");

				if (lc == null) {
				} else {
					for (String s : lc) {
						if (s.isEmpty() == false) {
							categorie.add(s);
						}
					}
					db.insertCategorie(categorie);
				}

				/* Liste des mots cl�s � ajouter */
				List<String> keyword = new LinkedList<String>();
				String[] lk = request.getParameterValues("keyword");
				if (lk == null) {
				} else {
					for (String s : lk) {

						if (s.isEmpty() == false) {
							keyword.add(s);
						}
					}
					db.insertKeyword(keyword);
				}

			} else if (form == 2) {
				Admin.generateAuditPDF("H:\\IMG\\audit.pdf", db);
				request.setAttribute("audit", "/image/audit.pdf");
			} else if (form == 3) {
				String login = request.getParameter("login");
				String pw = request.getParameter("password");
				if (login != null && pw != null) {
					/* On commence par connecter l'administrateur */
					String admin = db.getAdmin(login, Index.encode(pw));

					if (admin != null) {
						/*
						 * Remarque, TOUJOURS PAS DE TABLES ADMIN
						 */
						request.setAttribute("connect", 1);
						request.getSession().setAttribute("admin", admin);
					}
				}
			}

			request.setAttribute("produit", db.getAllProduit());
			request.setAttribute("luser", db.getAllClient());

			/** ------------------------- */

			this.getServletContext().getRequestDispatcher("/Admin.jsp")
					.forward(request, response);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
}
