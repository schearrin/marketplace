/**
 * 
 */
package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.Client;
import objets.ConnexionDB;

/**
 * @author Arutha
 * 
 */
public class UpdateClient extends HttpServlet {

	private static final long serialVersionUID = 2691776228381020408L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			
			int idc = 0;
			try {
				idc = Integer.parseInt(request.getParameter("idc"));
			} catch (NumberFormatException e) {
			}
			
			
			if (idc == 0) {
				request.setAttribute("e", "Ce client n'existe pas");
			} else {
				request.setAttribute("user", db.getClient(idc));
			}

			this.getServletContext().getRequestDispatcher("/Client.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
		} catch (SQLException e) {
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}

			int form = 0;
			try {
				form = Integer.parseInt(request.getParameter("form"));
			} catch (NumberFormatException e) {
			}

			if (form == 1) {
				int idc = 0; 
				try {
					idc = Integer.parseInt(request.getParameter("idc"));
				} catch (NumberFormatException e) {
				}
				
				String password = request.getParameter("password");
				if (password == null) {
					password = "ignore";
				} else {
					if (password.equalsIgnoreCase("password")) {
						password = "ignore";
					} else {
						password = Index.encode(password);
					}
				}
				String prenom = request.getParameter("prenom");
				String nom = request.getParameter("nom");
				String zip = request.getParameter("zip");
				String numero = request.getParameter("numero");
				String code = request.getParameter("code");

				int annee = 0, mois = 0;
				try {
					annee = Integer.parseInt(request.getParameter("annee"));
					mois = Integer.parseInt(request.getParameter("mois"));
				} catch (NumberFormatException e) {
				}
				String expire = annee + "-" + mois;

				
				/**
				 * Pour virer des cartes bancaires 
				 */
				List<Integer> lcb = new LinkedList<Integer>();
				String[] checkboxes = request.getParameterValues("supprcb");
				if (checkboxes == null) {
				} else {
					for (String s: checkboxes) {
						lcb.add(Integer.parseInt(s));
					}
					db.deleteUserCard(lcb);
				}

				/**
				 * Pour virer des images
				 */
				List<Integer> limg = new LinkedList<Integer>(); 
				checkboxes = request.getParameterValues("supprimg");
				if (checkboxes == null) {
				} else {
					for (String s: checkboxes) {
						limg.add(Integer.parseInt(s));
					}
					db.deleteUserImage(limg);
				}
				
				Client user =  db.updateClient(idc, password, prenom, nom,
						zip, numero, expire, code);
				request.setAttribute("user", user);
			}
			
			
			this.getServletContext().getRequestDispatcher("/Client.jsp")
					.forward(request, response);
		} catch (SQLException e) {
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
