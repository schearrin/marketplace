package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import objets.Categorie;
import objets.ConnexionDB;
import objets.Image;
import objets.Keyword;
import objets.Produit;

@WebServlet("/DOMExport")
public class DOMExport extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public DOMExport() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//En-t�te du document xml
		response.setContentType("application/xml");
		response.setCharacterEncoding("UTF-8");

		//Cr�ation d'un objet de classe document
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);


		try {

			//Recupere connexion
			ConnexionDB dbdomexp = (ConnexionDB) request.getSession().getAttribute("dbdomexp");
			if (dbdomexp == null) {

				dbdomexp = new ConnexionDB();
				request.getSession().setAttribute("dbdomexp", dbdomexp);

			}


			Produit all_pdt;
			List<Produit> liste_pdts = dbdomexp.getAllProduit();

			// Cr�e un arbre DOM correspondant � un documentXML

			DocumentBuilder db = factory.newDocumentBuilder();
			Document d = db.newDocument();

			Node produits = d.createElement("produits");
			d.appendChild(produits);

			for(Produit id_pdt : liste_pdts){
			
				all_pdt = dbdomexp.getProduit(id_pdt.getID_P());
				
				//produit
				Element produit = d.createElement("produit");
				produits.appendChild(produit);

				Attr id = d.createAttribute("id");
				id.setValue(Integer.toString(id_pdt.getID_P()));
				produit.setAttributeNode(id);
				
				//nom
				Element nom = d.createElement("nom");
				produit.appendChild(nom);
				Node text_nom = d.createTextNode(all_pdt.getNom());
				nom.appendChild(text_nom);
				
				//prix
				Element prix = d.createElement("prix");
				produit.appendChild(prix);
				Node text_prix = d.createTextNode(Float.toString(all_pdt.getPrix()));
				prix.appendChild(text_prix);
				
				//vendeur
				Element vendeur = d.createElement("vendeur");
				produit.appendChild(vendeur);
				Node text_vendeur = d.createTextNode(all_pdt.getVendeur());
				vendeur.appendChild(text_vendeur);
				
				
			
				//categories
				Element categories = d.createElement("categories");
				produit.appendChild(categories);
				
				List<Categorie> liste_cats = all_pdt.getCategorie();
				
				for(Categorie cat_pdt : liste_cats){
					Element categorie = d.createElement("categorie");
					categories.appendChild(categorie);
					Attr id_cat = d.createAttribute("id_cat");
					id_cat.setValue(Integer.toString(cat_pdt.getID_CAT()));
					categorie.setAttributeNode(id_cat);
					
					Element namec = d.createElement("name");
					categorie.appendChild(namec);
					Node text_namec = d.createTextNode(cat_pdt.getName());
					namec.appendChild(text_namec);
				}
				
				//keywords
				Element keywords = d.createElement("keywords");
				produit.appendChild(keywords);
				
				List<Keyword> liste_keys = all_pdt.getKeyword();
				
				for(Keyword key_pdt : liste_keys){
					Element keyword = d.createElement("keyword");
					keywords.appendChild(keyword);
					Attr id_kw = d.createAttribute("id_kw");
					id_kw.setValue(Integer.toString(key_pdt.getID_KW()));
					keyword.setAttributeNode(id_kw);
					
					Element namek = d.createElement("name");
					keyword.appendChild(namek);
					Node text_namek = d.createTextNode(key_pdt.getName());
					namek.appendChild(text_namek);
				}
				
				//image
				Element images = d.createElement("images");
				produit.appendChild(images);

				List<Image> liste_imgs = all_pdt.getImage();

				for(Image img_pdt : liste_imgs){
					Element image = d.createElement("image");
					images.appendChild(image);
					Attr id_i = d.createAttribute("id_i");
					id_i.setValue(Integer.toString(img_pdt.getID_I()));
					image.setAttributeNode(id_i);
					
					Element title = d.createElement("title");
					image.appendChild(title);
					Node text_title = d.createTextNode(img_pdt.getTitle());
					title.appendChild(text_title);
					
					Element path = d.createElement("path");
					image.appendChild(path);
					Node text_path = d.createTextNode(img_pdt.getChemin());
					path.appendChild(text_path);
				}
			}

			//Serialisation
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			transformer.transform(new DOMSource(d),
					new StreamResult(response.getWriter()));


			System.out.println("Ok DOMExport");

		} catch (Exception e) {
			System.out.println("Erreur DOMExport");
			e.printStackTrace();
			throw new ServletException(e);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
