package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.ConnexionDB;
import objets.Produit;

/**
 * Servlet implementation class FormulaireProduit
 */
@WebServlet("/FormulaireProduit")
public class FormulaireProduit extends HttpServlet {
	private static final long serialVersionUID = 1L;
       


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute("db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			
			
			request.setAttribute("categorie", db.getCategorie());
			request.setAttribute("keyword", db.getKeyword());
			
			this.getServletContext().getRequestDispatcher("/FormulaireProduit.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			new ServletException(e.fillInStackTrace());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* C'est ici que c'est intérressant */
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute("db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			
			
			request.setAttribute("categorie", db.getCategorie());
			request.setAttribute("keyword", db.getKeyword());
			
			
			/* --------------------------- */
			
			
			String Nom = request.getParameter("name"); 
			String Prix = request.getParameter("prix");
			String Vendeur = request.getParameter("vendeur"); 
			
			Float PrixFlottant = new Float (Prix); 
			
			List<Integer> lidc = new LinkedList<Integer>(); 
			String [] ls = request.getParameterValues("idc"); 
			if (ls == null){
				
			}else {
				for (String s : ls){
					lidc.add(Integer.parseInt(s));
				}
			}
			
			List<Integer> lidk = new LinkedList<Integer>(); 
			ls = request.getParameterValues("idk"); 
			if (ls == null){
				
			} else {
				for (String s : ls){
					lidk.add(Integer.parseInt(s)); 
				}
			}
			
			Produit p = db.insertProduit(Nom, PrixFlottant, Vendeur, lidc, lidk); 
			
			request.setAttribute("prod", p);
			
			/* ----------------------------- */
			this.getServletContext().getRequestDispatcher("/FormulaireProduit.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			new ServletException(e.fillInStackTrace());
		}	
	}

}
