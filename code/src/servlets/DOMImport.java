package servlets;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


public class DOMImport extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private String UPLOAD_DIRECTORY = "";

	public DOMImport(){
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		//Si le fichier import.xml n'existe pas alors :
		String path_file=getServletContext().getRealPath("/");
		File file = new File(path_file+"import.xml");
		System.out.println(path_file);
		if(!file.exists()){
			file.createNewFile();
			try{
				FileWriter writer = new FileWriter(file);
				writer.write("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				writer.write("\n");
				writer.write("<root>");
				writer.write("\n");
				writer.write("</root>");
				writer.close();
			}catch(Exception e){
				System.out.println("Erreur DOMImport : probl�me fichier import.xml");
			}
		}
		
		//Redirection vers la page jsp
		RequestDispatcher rd = request.getRequestDispatcher("/Import.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		//R�cup�re le chemin d'upload du fichier import.xml
		String path=getServletContext().getRealPath("/");
		UPLOAD_DIRECTORY=path;

		//Si encryption = multipart content
		if(ServletFileUpload.isMultipartContent(request)){
			try{

				//Releve tout les items de la requete dans une liste
				List<FileItem> multiparts = new ServletFileUpload(
						new DiskFileItemFactory()).parseRequest(request);

				for(FileItem item : multiparts){
					//Si c'est le type est un fichier et non autre chose dans formulaire
					if(!item.isFormField()){

						//Expression reguliere qui commence par caratere non blanc, fini par une extention xml et insensible a la casse
						String regex_xml = "([^\\s]+(\\.(?i)(xml))$)";

						String name = new File(item.getName()).getName();

						//Si le pattern xml est correct alors :
						if(Pattern.matches(regex_xml, name)){
							item.write( new File(UPLOAD_DIRECTORY + File.separator + "import.xml"));

						}else{
							request.setAttribute("not_xmlfile", "Erreur, ce n'est pas un fichier XML.");

							RequestDispatcher rd = request.getRequestDispatcher("/Import.jsp");;
							rd.forward(request, response);
						}
					}
				}

				System.out.println("Ok DOMImport");

			}catch(Exception e){
				System.out.println("Erreur DOMImport");
				e.printStackTrace();
				throw new ServletException(e);
			}
		}
		doGet(request, response);
	}

}
