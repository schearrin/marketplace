package servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.Categorie;
import objets.Client;
import objets.Commentaire;
import objets.ConnexionDB;
import objets.Keyword;
import objets.Produit;
import objets.ShoppingCartItem;
import objets.ShoppingCartManager;

/**
 * Servlet implementation class Info
 */

public class Info extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			
			int idp = 0;
			try {
				idp = Integer.parseInt(request.getParameter("idp"));
				Produit p = db.getProduit(idp);
				request.setAttribute("prod", p);
				request.setAttribute("similar", db.getProduitsSimilaires(p));
				
				Client client = (Client) request.getSession().getAttribute("user");
				if (client != null){
					int idc = client.getID_C();
					List<Commentaire> commentaires = p.getCommentaire(); 
					boolean bool = false; 
					for (Commentaire c : commentaires){
						if (c.getIdc() == idc) bool = true;  
					}
					/* On veut v�rifier que le client ID_C n'a pas encore laiss� de commentaire 
					 * Sur le produit ID_P, c'est pas trop demand�, �a va */
					if (bool == false) {
						request.setAttribute("comment", "Pas encore comment� par le client");
					}
					
				}
				
			} catch (NumberFormatException e) {
			}
			this.getServletContext().getRequestDispatcher("/Info.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}

			/* ----------------------------- */
			int form = 0;
			try {
				form = Integer.parseInt(request.getParameter("form"));

			} catch (NumberFormatException e) {

			}
			if (form == 1) {
				/* Pour afficher le formulaire */
				request.setAttribute("form", form);
				int idp = 0;
				try {
					idp = Integer.parseInt(request.getParameter("idp"));

					

					Produit p = db.getProduit(idp);
					request.setAttribute("prod", p);

					List<Categorie> produitCategories = p.getCategorie();
					List<Keyword> produitKeywords = p.getKeyword();

					List<Categorie> categories = db.getCategorie();
					List<Keyword> keywords = db.getKeyword();
					
					
					
					categories.removeAll(produitCategories); 
					keywords.removeAll(produitKeywords);

					
					request.setAttribute("categories",categories);
					System.out.println(categories.size() + " ? "
							+ produitCategories.size());

					request.setAttribute("keywords",keywords);

					System.out.println(keywords.size() + " ? "
							+ produitKeywords.size());
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			} else if (form == 2) {
				/* Pour ajouter un commentaires */
				int idp = 0; 
				int idc = 0; 
				int note = 0; 
				try {
					idp = Integer.parseInt(request.getParameter("idp"));
					idc = Integer.parseInt(request.getParameter("idc"));
					note = Integer.parseInt(request.getParameter("note"));
				} catch (NumberFormatException e){
					e.printStackTrace();
				}
				
				String text = request.getParameter("text"); 
				
				request.setAttribute("prod", db.insertCommentaire(idp, idc, note, text));
				request.setAttribute("similar", db.getProduitsSimilaires(db.getProduit(idp)));
			} else if (form == 3) {
				/* Pour traiter les UPDATE */
				int idp = 0;
				try {
					idp = Integer.parseInt(request.getParameter("idp"));
					String Name = request.getParameter("name");
					String Prix = request.getParameter("prix");
					Float PrixFlottant = new Float(Prix);
					String Vendeur = request.getParameter("vendeur");

					/* Changer les cat�gories */
					List<Integer> lidc = new LinkedList<Integer>();
					String[] ls = request.getParameterValues("idc");
					if (ls == null) {
					} else {
						for (String s : ls) {
							lidc.add(Integer.parseInt(s));
						}
					}

					/* Changer les mots cl�s */
					List<Integer> lidk = new LinkedList<Integer>();
					ls = request.getParameterValues("idk");
					if (ls == null) {
					} else {
						for (String s : ls) {
							lidk.add(Integer.parseInt(s));
						}
					}
					
					/**
					 * Pour virer des images
					 */
					List<Integer> limg = new LinkedList<Integer>(); 
					String [] checkboxes = request.getParameterValues("supprimg");
					if (checkboxes == null) {
					} else {
						for (String s: checkboxes) {
							limg.add(Integer.parseInt(s));
						}
						db.deleteProdImage(limg);
					}

					/* Finir */
					
					request.setAttribute("similar", db.getProduitsSimilaires(db.getProduit(idp)));
					request.setAttribute("prod", db.updateProduit(idp, Name, PrixFlottant, Vendeur, lidc, lidk));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			} else if (form == 4) {
				/* Ajouter dans le panier */
				int idp = 0;
				try {
					idp = Integer.parseInt(request.getParameter("idp"));
					
					
					int Quantite = Integer.parseInt(request.getParameter("howmuch"));
					
					ShoppingCartItem panierItem = new ShoppingCartItem(db.getProduit(idp), Quantite); 
					ShoppingCartManager panier = (ShoppingCartManager) request.getSession().getAttribute(
							"panier");
					if (panier == null) {
						panier = new ShoppingCartManager();
					}
					
					panier.addProduit(panierItem);
					
					
					request.setAttribute("prod", db.getProduit(idp));
					request.setAttribute("similar", db.getProduitsSimilaires(db.getProduit(idp)));
					
					request.getSession().setAttribute("panier", panier);
				}catch (NumberFormatException e){
					e.printStackTrace();
				}
			} else if (form == 5) {
				/* Editer un commentaire */
				int idp = 0;
				int idc = 0; 
				
				try {
					idp = Integer.parseInt(request.getParameter("idp"));
					idc = Integer.parseInt(request.getParameter("idc"));
					String text = request.getParameter("text"); 
					
					request.setAttribute("prod", db.updateCommentaire(idp, idc, text));	
					
					request.setAttribute("similar", db.getProduitsSimilaires(db.getProduit(idp)));
				}catch (NumberFormatException e){
					e.printStackTrace();
				}
			}

			/* ----------------------------- */

			this.getServletContext().getRequestDispatcher("/Info.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
