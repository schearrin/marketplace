/**
 * 
 */
package servlets;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.Client;
import objets.ConnexionDB;
import objets.Produit;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.pdf.PdfWriter;



/**
 * 2016-12-19
 * @author Arutha Classe qui controle la liste de vid�o affich�e
 */
public class Liste extends HttpServlet {

	/**
	 * G�n�re le PDF path � partir de la liste lv
	 * 
	 * @param path
	 * @param lv
	 */
	protected static void generatePDF(String path, java.util.List<Produit> lv) {

		Document document = new Document();
		File MyFile = new File(path);
		MyFile.delete();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(path, false));
			document.open();
			List orderedList = new List(List.ORDERED);
			for (Produit v : lv) {
				orderedList.add(new ListItem(v.getNom() + " ( "
						+ v.getPrix() + " EUR, "
						+ v.getVendeur() + " )"));
			}
			document.add(orderedList);
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	protected static void exportDB(ConnexionDB db) {
		java.util.List<String> DATA = new LinkedList<String>(); 
		File f = new File("H:\\IMG\\exportDB.sql"); 
		f.delete(); 
		try {
			DATA.add(db.exportProduit()); 
			DATA.add(db.exportClient()); 
			
			DATA.add(db.exportCartes()); 
			DATA.add(db.exportCategorie()); 
			DATA.add(db.exportImage());
			DATA.add(db.exportKeyword()); 
			
			DATA.add(db.exportAchat()); 
			DATA.add(db.exportCommentaire());
			
			DATA.add(db.exportLinkClientCB()); 
			DATA.add(db.exportLinkClientImage()); 
			DATA.add(db.exportLinkProdCat()); 
			DATA.add(db.exportLinkProdImage()); 
			DATA.add(db.exportLinkProdKW()); 
		
			PrintWriter PW = new PrintWriter(new BufferedWriter(new FileWriter(f))); 
			for (String d:DATA){
				PW.println (d.replace(",\n;",";") + "\n --------------- \n"); 
			}
			PW.close();
		
		} catch (SQLException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute("db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("count", db.getNombreProduit());
			request.setAttribute("produit", db.getAllProduit());
			LinkedList<Client> lc = (LinkedList<Client>) db.getAllClient();
			request.setAttribute("client", lc);
			
			
			this.getServletContext().getRequestDispatcher("/Liste.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute("db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			request.setAttribute("count", db.getNombreProduit());
			request.setAttribute("produit", db.getAllProduit());
			
			
			/*--------------------------------------------*/
			
			int form = 0; 
			try {
				form = Integer.parseInt(request.getParameter("form"));
			} catch (NumberFormatException e){
			}
			
			if (form == 1){
				generatePDF("H:\\IMG\\catalogue.pdf", db.getAllProduit());
				request.setAttribute("success", "/image/catalogue.pdf");
			} else if (form == 2) {
				exportDB(db);
				request.setAttribute("success", "/image/exportDB.sql");
			} else if (form == 3){
				/* exporte en xml */
				request.setAttribute("success", "/image/produits.xml");
			}
			
			/*--------------------------------------------*/
			this.getServletContext().getRequestDispatcher("/Liste.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static final long serialVersionUID = -1505588310660082735L;
}
