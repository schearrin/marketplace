/**
 * 
 */
package servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import objets.Client;
import objets.ConnexionDB;
import objets.ShoppingCartManager;

/**
 * @author Arutha
 * 
 */
public class Panier extends HttpServlet {

	private static final long serialVersionUID = 2691776228381020408L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}
			Client user = (Client) request.getSession().getAttribute("user");
			if (user == null) {
				request.setAttribute("erreur", "Pas de connexion");
			}
			
			ShoppingCartManager Panier = (ShoppingCartManager) request.getSession().getAttribute("panier");
			
			
			
			
			this.getServletContext().getRequestDispatcher("/Panier.jsp")
					.forward(request, response);
		} catch (ClassNotFoundException e) {
		} catch (SQLException e) {
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionDB db = (ConnexionDB) request.getSession().getAttribute(
					"db");
			if (db == null) {
				db = new ConnexionDB();
				request.getSession().setAttribute("db", db);
			}

			Client user = (Client) request.getSession().getAttribute("user");
			if (user == null) {
				request.setAttribute("erreur",
						"<p>Vous n'�tes pas connect�</p>");
			}
			
			int form = 0; 
			try {
				form = Integer.parseInt(request.getParameter("form")); 
				ShoppingCartManager Panier = (ShoppingCartManager) request.getSession().getAttribute(
						"panier");
				
				int ID_C = user.getID_C(); 
				
				
				/*
				 * Achat (ID_P, ID_C, Quantite, Date_Achat) 
				 * */
				
				user =  db.insertAchat (ID_C, Panier.getPanier()); 
				
				request.getSession().removeAttribute("panier");
				
			} catch ( NumberFormatException e) {
				e.printStackTrace();
			}
			
			request.getSession().setAttribute("user", user);
			this.getServletContext().getRequestDispatcher("/Panier.jsp")
					.forward(request, response);
		} catch (SQLException e) {
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
