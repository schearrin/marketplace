package objets;

/**
 * 2016-12-28
 * @author Arutha
 *
 */
public class ShoppingCartItem {
	private Produit ID_P; 
	private int quantite; 
	private float prix;
	
	public ShoppingCartItem (Produit idp, int q) {
		setID_P(idp);
		setQuantite(q);
		setPrix(idp.getPrix() * q);
	}
	
	/**
	 * @return the iD_P
	 */
	public Produit getID_P() {
		return ID_P;
	}
	/**
	 * @param iD_P the iD_P to set
	 */
	public void setID_P(Produit iD_P) {
		ID_P = iD_P;
	}
	/**
	 * @return the quantite
	 */
	public int getQuantite() {
		return quantite;
	}
	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	/**
	 * @return the prix
	 */
	public float getPrix() {
		return prix;
	}
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(float prix) {
		this.prix = prix;
	} 
	

}
