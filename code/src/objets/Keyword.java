package objets;

public class Keyword {
	private int ID_KW;
	private String name;

	public Keyword(int i, String name) {
		this.ID_KW = i;
		this.name = name;
	}

	@Override 
	public boolean equals (Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (!(o instanceof Keyword))return false;
		Keyword v = (Keyword) o;
		return (this.ID_KW == v.ID_KW);
	}
	
	/**
	 * @return the iD_KW
	 */
	public int getID_KW() {
		return ID_KW;
	}

	/**
	 * @param iD_KW
	 *            the iD_KW to set
	 */
	public void setID_KW(int iD_KW) {
		ID_KW = iD_KW;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
