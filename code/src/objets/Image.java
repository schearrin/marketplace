package objets;

public class Image {
	private int ID_I; 
	private String chemin;
	private String title; 
	
	public Image (int i, String path, String string){
		setID_I(i);
		setChemin(path);
		setTitle(string);
	}



	/**
	 * @return the iD_I
	 */
	public int getID_I() {
		return ID_I;
	}

	/**
	 * @param iD_I the iD_I to set
	 */
	public void setID_I(int iD_I) {
		ID_I = iD_I;
	}



	/**
	 * @return the chemin
	 */
	public String getChemin() {
		return chemin;
	}



	/**
	 * @param chemin the chemin to set
	 */
	public void setChemin(String chemin) {
		this.chemin = chemin;
	}



	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}



	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}



	
}
