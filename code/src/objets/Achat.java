package objets;

public class Achat {
	private int IDP; 
	private String dateAchat; 
	private String produit; 
	private int quantite; 
	
	public Achat(String s, Produit p, int i) {
		setIDP(p.getID_P());
		setDateAchat(s);
		setProduit(p.getNom());
		setQuantite(i);
	}

	/**
	 * @return the dateAchat
	 */
	public String getDateAchat() {
		return dateAchat;
	}

	/**
	 * @param dateAchat the dateAchat to set
	 */
	public void setDateAchat(String dateAchat) {
		this.dateAchat = dateAchat;
	}

	/**
	 * @return the produit
	 */
	public String getProduit() {
		return produit;
	}

	/**
	 * @param produit the produit to set
	 */
	public void setProduit(String produit) {
		this.produit = produit;
	}

	/**
	 * @return the iDP
	 */
	public int getIDP() {
		return IDP;
	}

	/**
	 * @param iDP the iDP to set
	 */
	public void setIDP(int iDP) {
		IDP = iDP;
	}

	/**
	 * @return the quantite
	 */
	public int getQuantite() {
		return quantite;
	}

	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
}
