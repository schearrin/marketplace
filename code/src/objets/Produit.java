package objets;

import java.util.List;


/**
 * 2016-12-17
 * @author Arutha
 * Classe Produit
 */
public class Produit {
	private int ID_P; 
	private String nom; 
	private Float prix; 
	private String vendeur; 
	
	private int notation; 
	
	private List<Categorie> categorie; 
	private List<Keyword> keyword;
	
	private List<Commentaire> commentaire; 
	private List<Image> image; 
	
	/**
	 * 
	 * @param IDP
	 * @param nom
	 * @param prix
	 * @param vendeur
	 * @param car
	 * @param kw
	 * @param c
	 * @param im
	 */
	public Produit (int IDP, String nom, Float prix, String vendeur, 
			List<Categorie> car, List<Keyword> kw, List<Commentaire> c, List<Image> im, 
			int notation){
		setID_P(IDP);
		setNom(nom);
		setPrix(prix);
		setVendeur(vendeur);
		setCategorie(car);
		setKeyword(kw);
		setCommentaire(c);
		setImage(im);
		setNotation(notation);
	}
	
	
	@Override 
	public boolean equals (Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (!(o instanceof Produit))return false;
		Produit v = (Produit) o;
		return (this.ID_P == v.ID_P);
	}
	
	
	/**
	 * @return the iD_P
	 */
	public int getID_P() {
		return ID_P;
	}
	/**
	 * @param iD_P the iD_P to set
	 */
	public void setID_P(int iD_P) {
		ID_P = iD_P;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prix
	 */
	public Float getPrix() {
		return prix;
	}
	/**
	 * @param prix the prix to set
	 */
	public void setPrix(Float prix) {
		this.prix = prix;
	}
	/**
	 * @return the vendeur
	 */
	public String getVendeur() {
		return vendeur;
	}
	/**
	 * @param vendeur the vendeur to set
	 */
	public void setVendeur(String vendeur) {
		this.vendeur = vendeur;
	}
	/**
	 * @return the categorie
	 */
	public List<Categorie> getCategorie() {
		return categorie;
	}
	/**
	 * @param categorie the categorie to set
	 */
	public void setCategorie(List<Categorie> categorie) {
		this.categorie = categorie;
	}
	/**
	 * @return the keyword
	 */
	public List<Keyword> getKeyword() {
		return keyword;
	}
	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(List<Keyword> keyword) {
		this.keyword = keyword;
	}
	/**
	 * @return the commentaire
	 */
	public List<Commentaire> getCommentaire() {
		return commentaire;
	}
	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(List<Commentaire> commentaire) {
		this.commentaire = commentaire;
	}
	/**
	 * @return the image
	 */
	public List<Image> getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(List<Image> image) {
		this.image = image;
	}


	/**
	 * @return the notation
	 */
	public int getNotation() {
		return notation;
	}


	/**
	 * @param notation the notation to set
	 */
	public void setNotation(int notation) {
		this.notation = notation;
	} 
	
	
}
