package objets;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * 2016-12-17
 * 
 * @author Arutha Classe Connexion DATA BASE
 */
public class ConnexionDB {

	private Connection conn;

	public ConnexionDB() throws ClassNotFoundException, SQLException {
		Class.forName("org.postgresql.Driver");
		
		int i=1;
		conn = null;

		while(true){
			try{
				if(i==1){
					conn = DriverManager.getConnection(
							"jdbc:postgresql://localhost:5432/MIAGE", "postgres",
							"argonath");
				}
				if(i==2){
					conn = DriverManager.getConnection("jdbc:postgresql:projet-marketplace", "postgres", "1234");
				}
				break;

			}catch(Exception e){
				if(i>=2){
					System.out.println("Erreur de connexion");
					break;
				}else{
					i++;
				}
			}
		}

	}

	@Override
	protected void finalize() {
		try {
			if (conn != null && !conn.isClosed())
				conn.close();
		} catch (Exception e) {
		}
	}

	/**
	 * 
	 * @return List de tous les clients
	 * @throws SQLException
	 */
	public List<Client> getAllClient() throws SQLException {

		PreparedStatement s = conn.prepareStatement("SELECT id_c FROM client");
		ResultSet r = s.executeQuery();
		List<Client> lc = new LinkedList<Client>();
		while (r.next()) {
			lc.add(getClient(r.getInt(1)));
		}
		return lc;
	}

	/**
	 * 
	 * @return List de tous les produits
	 * @throws SQLException
	 */
	public List<Produit> getAllProduit() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT id_p FROM produit");
		ResultSet r = s.executeQuery();
		List<Produit> lp = new LinkedList<Produit>();
		while (r.next()) {
			lp.add(getProduit(r.getInt(1)));
		}
		return lp;
	}

	/**
	 * 
	 * @param ID_C
	 * @return Le Client ID_C
	 * @throws SQLException
	 */
	public Client getClient(int ID_C) throws SQLException {
		/* Les Cartes */
		PreparedStatement s = conn.prepareStatement("SELECT "
				+ "cartes.numero, " + "cartes.expire, "
				+ "cartes.code, cartes.ID_CB FROM " + "public.client, "
				+ "public.link_client_cb, " + "public.cartes WHERE "
				+ "link_client_cb.id_c = client.id_c AND "
				+ "cartes.id_cb = link_client_cb.id_cb AND "
				+ "client.id_c = ?;");
		s.setInt(1, ID_C);
		ResultSet r = s.executeQuery();
		List<Carte> lc = new LinkedList<Carte>();
		while (r.next()) {
			lc.add(new Carte(r.getInt(4), r.getString(1), r.getString(2), r
					.getString(3)));
		}
		r.close();
		s.close();

		/* Les images */
		s = conn.prepareStatement("SELECT " + "image.id_i, image.path, "
				+ "image.title FROM " + "public.image, "
				+ "public.client, " + "public.link_client_image WHERE "
				+ "client.id_c = link_client_image.id_c AND "
				+ "link_client_image.id_i = image.id_i AND "
				+ "client.id_c = ?");
		s.setInt(1, ID_C);
		r = s.executeQuery();
		List<Image> li = new LinkedList<Image>();
		while (r.next()) {
			li.add(new Image(r.getInt(1), r.getString(2), r.getString(3)));
		}
		/* Le title est pas encore utilise */
		r.close();
		s.close();

		/* Les achats */
		s = conn.prepareStatement("SELECT " + "achat.date_achat, "
				+ "achat.id_p, achat.quantite FROM " + "public.client, "
				+ "public.achat WHERE " + "client.id_c = achat.id_c AND "
				+ "client.id_c = ?");
		s.setInt(1, ID_C);
		r = s.executeQuery();
		List<Achat> la = new LinkedList<Achat>();
		while (r.next()) {
			la.add(new Achat(r.getDate(1).toString(), getProduit(r.getInt(2)), r.getInt(3)));
		}
		r.close();
		s.close();

		/* Informations basiques */
		s = conn.prepareStatement("SELECT client.login, " + "client.hash_pw, "
				+ "client.nom, " + "client.prenom, " + "client.zip FROM "
				+ "public.client WHERE " + "client.id_c = ?");
		s.setInt(1, ID_C);
		r = s.executeQuery();
		Client c = null;
		if (r.next()) {

			c = new Client(r.getString(1), ID_C, r.getString(2),
					r.getString(3), r.getString(4), r.getString(5), li, lc, la);
		} else {
			throw new SQLException();
		}
		r.close();
		s.close();
		return c;

	}

	/**
	 * 
	 * @param ID_P
	 * @return Le produit ID_P
	 * @throws SQLException
	 */
	public Produit getProduit(int ID_P) throws SQLException {
		/* Les categories */
		PreparedStatement s = conn.prepareStatement("SELECT   "
				+ "categorie.id_cat, " + "categorie.name FROM "
				+ "public.categorie, " + "public.produit, "
				+ "public.link_prod_cat WHERE   "
				+ "categorie.id_cat = link_prod_cat.id_cat AND   "
				+ "link_prod_cat.id_p = produit.id_p AND   "
				+ "produit.id_p = ?");
		s.setInt(1, ID_P);
		ResultSet r = s.executeQuery();
		List<Categorie> lcat = new LinkedList<Categorie>();
		while (r.next()) {
			lcat.add(new Categorie(r.getInt(1), r.getString(2)));
		}
		r.close();
		s.close();

		/* Les mots cles */

		s = conn.prepareStatement("SELECT " + "keyword.id_kw, "
				+ "keyword.name FROM " + "public.keyword, "
				+ "public.produit, " + "public.link_prod_kw WHERE "
				+ "keyword.id_kw = link_prod_kw.id_kw AND "
				+ "link_prod_kw.id_p = produit.id_p AND "
				+ "produit.id_p = ?; ");
		/* requ�te fonctionne */
		s.setInt(1, ID_P);
		r = s.executeQuery();
		List<Keyword> lkw = new LinkedList<Keyword>();
		while (r.next()) {
			lkw.add(new Keyword(r.getInt(1), r.getString(2)));
		}
		r.close();
		s.close();

		/* Les images */
		s = conn.prepareStatement("SELECT " + "image.id_i, "
				+ "image.path, image.title FROM " + "public.produit, "
				+ "public.image, public.link_prod_image WHERE "
				+ "image.id_i = link_prod_image.id_i AND "
				+ "link_prod_image.id_p = produit.id_p AND "
				+ "produit.id_p = ?");

		s.setInt(1, ID_P);
		r = s.executeQuery();
		List<Image> li = new LinkedList<Image>();
		while (r.next()) {
			li.add(new Image(r.getInt(1), r.getString(2), r.getString(3)));
		}
		r.close();
		s.close();

		/* Les commentaires */
		s = conn.prepareStatement("SELECT " + "client.prenom, "
				+ "commentaire.texte, " + "commentaire.date_com, "
				+ "commentaire.note, " + "client.id_c FROM "
				+ "public.commentaire, " + "public.client, "
				+ "public.produit WHERE "
				+ "commentaire.id_c = client.id_c AND "
				+ "produit.id_p = commentaire.id_p AND " + "produit.id_p = ?");
		s.setInt(1, ID_P);
		r = s.executeQuery();
		List<Commentaire> lcomm = new LinkedList<Commentaire>();
		while (r.next()) {
			lcomm.add(new Commentaire(r.getString(1), r.getString(2), r
					.getInt(4), r.getDate(3).toString(), r.getInt(5)));
		}
		r.close();
		s.close();

		/* La note moyenne des commentaires  */
		int notation = 5;
		s = conn.prepareStatement("SELECT avg(commentaire.note) AS MOYENNE FROM public.commentaire, public.produit "
				+ "WHERE produit.id_p = commentaire.id_p AND produit.id_p = ?;  "); 
		s.setInt(1, ID_P);
		r = s.executeQuery();
		if (r.next()){
			Float f = r.getFloat(1); 
			notation = f.intValue(); 
		} else {
			notation = 0; 
		}
		r.close();
		s.close();

		/* Les informations importantes */
		s = conn.prepareStatement("SELECT " + "produit.nom, "
				+ "produit.prix, " + "produit.vendeur FROM "
				+ "public.produit WHERE " + "produit.id_p = ?;");
		s.setInt(1, ID_P);
		r = s.executeQuery();
		Produit p = null;
		if (r.next()) {

			p = new Produit(ID_P, r.getString(1), r.getFloat(2),
					r.getString(3), lcat, lkw, lcomm, li, notation);

		} else {
			throw new SQLException();
		}
		return p;
	}

	/**
	 * M�thode qui renvoie la liste des keywords
	 * 
	 * @return toute la liste des KEYWORD
	 * @throws SQLException
	 */
	public List<Keyword> getKeyword() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT * FROM KEYWORD");
		ResultSet res = s.executeQuery();
		List<Keyword> lst = new LinkedList<Keyword>();
		while (res.next()) {
			lst.add(new Keyword(res.getInt(1), res.getString(2)));
		}
		return lst;
	}

	/**
	 * M�thode qui renvoie les cat�gories
	 * 
	 * @return toute la liste des CATEGORIE, pour la mettre dans un select par
	 *         exemple
	 * @throws SQLException
	 */
	public List<Categorie> getCategorie() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT * FROM CATEGORIE");
		ResultSet res = s.executeQuery();
		List<Categorie> lst = new LinkedList<Categorie>();
		while (res.next()) {
			lst.add(new Categorie(res.getInt(1), res.getString(2)));
		}
		return lst;
	}

	/**
	 * 
	 * @return Le nombre de produit dans la base
	 * @throws SQLException
	 */
	public int getNombreProduit() throws SQLException {
		PreparedStatement s = conn
				.prepareStatement("SELECT count(*) FROM PRODUIT");
		ResultSet r = s.executeQuery();
		int i = 0;
		if (r.next()) {
			i = r.getInt(1);
		} else {
			throw new SQLException();
		}
		return i;
	}

	/**
	 * 2016-12-19 Remarque, les images sont trait�es � part.
	 * 
	 * @param nom
	 * @param prix
	 * @param vendeur
	 * @param categories
	 * @param keywords
	 * @param Image
	 * @return le nouveau Produit
	 * @throws SQLException
	 */
	public Produit insertProduit(String nom, float prix, String vendeur,
			List<Integer> categories, List<Integer> keywords)
					throws SQLException {
		/* Insertion Principale */
		PreparedStatement s = conn
				.prepareStatement("INSERT INTO PRODUIT (ID_P, NOM, PRIX, VENDEUR) "
						+ "VALUES (nextval('serial') , ?, ?, ?) RETURNING ID_P");

		s.setString(1, nom);
		s.setFloat(2, prix);
		s.setString(3, vendeur);
		ResultSet r = s.executeQuery();
		int idp = 0;
		if (r.next()) {
			idp = r.getInt(1);
		}
		r.close();
		s.close();

		/* L�, on ins�re les liens vers les cat�gories */
		s = conn.prepareStatement("INSERT INTO LINK_PROD_CAT (ID_CAT, ID_P) "
				+ "VALUES (?, ?)");
		s.setInt(2, idp);
		for (int id_cat : categories) {
			s.setInt(1, id_cat);
			s.execute();
		}
		s.close();

		/* L�, on ins�re les liens vers les keywords */
		s = conn.prepareStatement("INSERT INTO LINK_PROD_KW (ID_KW, ID_P) "
				+ "VALUES (?, ?)");
		s.setInt(2, idp);
		for (int id_kw : keywords) {
			s.setInt(1, id_kw);
			s.execute();
		}

		return getProduit(idp);

	}

	/**
	 * 
	 * @param login
	 * @param hash_mp
	 * @param nom
	 * @param prenom
	 * @param zip
	 * @return le nouveau Client
	 * @throws SQLException
	 */
	public Client insertClient(String login, String hash_mp, String nom,
			String prenom, String zip) throws SQLException {
		PreparedStatement s = conn
				.prepareStatement("INSERT INTO CLIENT (LOGIN, ID_C, HASH_PW, NOM, PRENOM, ZIP) "
						+ "VALUES (?, nextval('serial'), ?, ?, ?, ?) RETURNING ID_C");
		s.setString(1, login);
		s.setString(2, hash_mp);
		s.setString(3, prenom);
		s.setString(4, nom);
		s.setString(5, zip);
		ResultSet r = s.executeQuery();
		int idc = 0;
		if (r.next()) {
			idc = r.getInt(1);
		}
		return getClient(idc);

	}

	/**
	 * 
	 * @param idp
	 * @param name
	 * @param prixFlottant
	 * @param vendeur
	 * @param lidc
	 * @param lidk
	 * @return Le produit mis � jour
	 * @throws SQLException
	 */
	public Produit updateProduit(int idp, String name, Float prixFlottant,
			String vendeur, List<Integer> lidc, List<Integer> lidk)
					throws SQLException {

		/* On commence par DELETE dans les tables de liens */
		PreparedStatement s = conn
				.prepareStatement("DELETE FROM LINK_PROD_CAT WHERE ID_P = ?");
		s.setInt(1, idp);
		s.execute();
		s = conn.prepareStatement("DELETE FROM LINK_PROD_KW WHERE ID_P = ?");
		s.setInt(1, idp);
		s.execute();
		s.close();

		/* Pour INSERT ensuite avec des listes fraiches */
		s = conn.prepareStatement("INSERT INTO LINK_PROD_CAT (ID_CAT, ID_P) VALUES (?, ?)");
		s.setInt(2, idp);
		for (int i : lidc) {
			s.setInt(1, i);
			s.execute();
		}
		s.close();

		s = conn.prepareStatement("INSERT INTO LINK_PROD_KW (ID_KW, ID_P) VALUES (?, ?) ");
		s.setInt(2, idp);
		for (int i : lidk) {
			s.setInt(1, i);
			s.execute();
		}
		s.close();

		s = conn.prepareStatement("UPDATE PRODUIT SET NOM = ?, PRIX = ?, VENDEUR = ? WHERE ID_P = ? RETURNING ID_P");
		s.setString(1, name);
		s.setFloat(2, prixFlottant);
		s.setString(3, vendeur);
		s.setInt(4, idp);

		ResultSet r = s.executeQuery();
		Produit p = null;
		if (r.next()) {
			p = getProduit(r.getInt(1));
		} else {
			new SQLException();
		}
		return p;
	}

	/**
	 * 
	 * @param tITRE 
	 * @param iD
	 * @param string
	 * @throws SQLException
	 */
	public void insertImageClient(String TITRE, int iD, String string) throws SQLException {
		PreparedStatement s = conn
				.prepareStatement("INSERT INTO IMAGE (ID_I, PATH, TITLE) VALUES (nextval('serial'), ?, ?) RETURNING ID_I");
		s.setString(1, string);
		s.setString(2, TITRE); 
		ResultSet r = s.executeQuery();
		s = conn.prepareStatement("INSERT INTO LINK_CLIENT_IMAGE (ID_I, ID_C) VALUES (?, ?) ");
		if (r.next()) {
			s.setInt(2, iD);
			s.setInt(1, r.getInt(1));
			s.execute();
		}
	}

	/**
	 * 
	 * @param iD
	 * @param string
	 * @throws SQLException
	 */
	public void insertImageProduit(String TITRE, int iD, String string) throws SQLException {
		PreparedStatement s = conn
				.prepareStatement("INSERT INTO IMAGE (ID_I, PATH, TITLE) VALUES(nextval('serial'), ?, ?) RETURNING ID_I");
		s.setString(1, string);
		s.setString(2, TITRE);
		ResultSet r = s.executeQuery();
		s = conn.prepareStatement("INSERT INTO LINK_PROD_IMAGE (ID_I, ID_P) VALUES (?, ?) ");
		if (r.next()) {
			s.setInt(2, iD);
			s.setInt(1, r.getInt(1));
			s.execute();
		}

	}

	/**
	 * 
	 * @param login
	 * @param encode
	 * @return Le Client login si le password encode est correct
	 * @throws SQLException
	 */
	public Client getUser(String login, String encode) throws SQLException {
		Client user = null;
		PreparedStatement s = conn
				.prepareStatement("SELECT ID_C FROM CLIENT WHERE LOGIN = ? AND HASH_PW = ?");
		s.setString(1, login);
		s.setString(2, encode);
		ResultSet r = s.executeQuery();
		if (r.next()) {
			int ID_C = r.getInt(1);
			user = getClient(ID_C);
		}
		return user;
	}

	/**
	 * 
	 * @param idp
	 * @param idc
	 * @param note
	 * @param text
	 * @return le Produit avec le nouveau Commentaire
	 * @throws SQLException
	 */
	public Produit insertCommentaire(int idp, int idc, int note, String text)
			throws SQLException {
		PreparedStatement s = conn
				.prepareStatement("INSERT INTO COMMENTAIRE (ID_P, ID_C, TEXTE, NOTE, DATE_COM) "
						+ "VALUES (?, ?, ?, ?, current_timestamp) RETURNING ID_P");
		s.setInt(1, idp);
		s.setInt(2, idc);
		s.setString(3, text);
		s.setInt(4, note);

		ResultSet r = s.executeQuery();
		Produit p = null;
		if (r.next()) {
			p = getProduit(r.getInt(1));
		}
		return p;
	}

	/**
	 * 
	 * @param id_C
	 * @param numero
	 * @param expire
	 * @param code
	 * @return Le Client avec sa nouvelle Carte
	 * @throws SQLException
	 */
	public Client insertCarteBancaire(int id_C, String numero, String expire,
			String code) throws SQLException {
		boolean condition = false;
		if (numero.length() > 16)
			condition = true;
		if (code.length() > 3)
			condition = true;

		if (condition) {
			return getClient(id_C);
		} else {
			PreparedStatement s = conn
					.prepareStatement("INSERT INTO CARTES (ID_CB, NUMERO, EXPIRE, CODE) "
							+ "VALUES (nextval('serial'), ?, ?, ?) RETURNING ID_CB");
			s.setString(1, numero);
			s.setString(2, expire);
			s.setString(3, code);
			ResultSet r = s.executeQuery();

			int ID_CB;
			if (r.next()) {
				ID_CB = r.getInt(1);
				PreparedStatement s1 = conn
						.prepareStatement("INSERT INTO LINK_CLIENT_CB (ID_CB, ID_C) "
								+ "VALUES (?, ?)");
				s1.setInt(1, ID_CB);
				s1.setInt(2, id_C);
				s1.execute();
				s1.close();
			}
			r.close();
			s.close();
			return getClient(id_C);
		}
	}

	/**
	 * 
	 * @param id_C
	 * @param lcb
	 * @throws SQLException
	 */
	public void deleteUserCard(List<Integer> lcb) throws SQLException {
		if (!lcb.isEmpty()) {
			PreparedStatement s = conn
					.prepareStatement("DELETE FROM LINK_CLIENT_CB WHERE ID_CB = ?");
			for (int i : lcb) {
				s.setInt(1, i);
			}
			s.execute();
			s = conn.prepareStatement("DELETE FROM CARTES WHERE ID_CB = ?");
			for (int i : lcb) {
				s.setInt(1, i);
			}
			s.execute();
			s.close();
		}
	}

	/**
	 * 
	 * @param id_C
	 * @param password
	 * @param prenom
	 * @param nom
	 * @param zip
	 * @param numero
	 * @param expire
	 * @param code
	 * @return le Client mis � jour
	 * @throws SQLException
	 */
	public Client updateClient(int id_C, String password, String prenom,
			String nom, String zip, String numero, String expire, String code)
					throws SQLException {
		Client user = null;
		PreparedStatement s;

		if (password.equalsIgnoreCase("ignore")) {
			s = conn.prepareStatement("UPDATE CLIENT SET PRENOM = ?, NOM = ?, ZIP = ? WHERE ID_C = ?");
			s.setString(1, prenom);
			s.setString(2, nom);
			s.setString(3, zip);
			s.setInt(4, id_C);
		} else {
			s = conn.prepareStatement("UPDATE CLIENT SET HASH_PW = ?, PRENOM = ?, NOM = ?, ZIP = ? WHERE ID_C = ?");
			s.setString(1, password);
			s.setString(2, prenom);
			s.setString(3, nom);
			s.setString(4, zip);
			s.setInt(5, id_C);
		}
		s.execute();
		s.close();
		user = insertCarteBancaire(id_C, numero, expire, code);

		return user;
	}

	/**
	 * 
	 * @param limg les images � supprimer
	 * @throws SQLException
	 */
	public void deleteUserImage(List<Integer> limg) throws SQLException {
		PreparedStatement s = conn
				.prepareStatement("DELETE FROM LINK_CLIENT_IMAGE WHERE ID_I = ?");
		for (int i : limg) {
			s.setInt(1, i);
			s.execute();
		}
		s.close();
		/*
		 * peut causer un petit probl�me si une image est dans les deux tables
		 * LINK_CLIENT_IMAGE et LINK_PROD_IMAGE, au pire, on peut mettre un if
		 */
		s = conn.prepareStatement("DELETE FROM IMAGE WHERE ID_I = ?");
		for (int i : limg) {
			s.setInt(1, i);
			s.execute();
		}
		s.close();
	}

	/**
	 * 
	 * @param limg les images � supprimer
	 * @throws SQLException
	 */
	public void deleteProdImage(List<Integer> limg) throws SQLException {
		PreparedStatement s = conn
				.prepareStatement("DELETE FROM LINK_PROD_IMAGE WHERE ID_I = ?");
		for (int i : limg) {
			s.setInt(1, i);
			s.execute();
		}
		s.close();
		/*
		 * peut causer un petit probl�me si une image est dans les deux tables
		 * LINK_CLIENT_IMAGE et LINK_PROD_IMAGE, au pire, on peut mettre un if
		 */
		s = conn.prepareStatement("DELETE FROM IMAGE WHERE ID_I = ?");
		for (int i : limg) {
			s.setInt(1, i);
			s.execute();
		}
		s.close();
	}

	/**
	 * 
	 * @param lproduit
	 * @throws SQLException
	 */
	public void deleteProduits(List<Integer> lproduit) throws SQLException {
		PreparedStatement s = conn.prepareStatement("DELETE FROM LINK_PROD_CAT WHERE ID_P = ? ");
		for (int idp:lproduit){
			s.setInt(1, idp);
			s.execute();
		}
		s.close();
		s = conn.prepareStatement("DELETE FROM LINK_PROD_KW WHERE ID_P = ?"); 
		for (int idp:lproduit){
			s.setInt(1, idp);
			s.execute();
		}
		s.close();
		/* ON DELETE AUSSI DANS LA TABLE IMAGE*/
		s = conn.prepareStatement("DELETE FROM LINK_PROD_IMAGE WHERE ID_P = ? RETURNING ID_I");
		for (int idp:lproduit){
			s.setInt(1, idp);
			ResultSet r = s.executeQuery(); 
			if (r.next()){
				PreparedStatement s1 = conn.prepareStatement("DELETE FROM IMAGE WHERE ID_I = ?");
				s1.setInt(1, r.getInt(1));
				s1.execute();
				s1.close();
			}
		}
		s.close();
		s = conn.prepareStatement("DELETE FROM ACHAT WHERE ID_P = ?");
		for (int idp:lproduit){
			s.setInt(1, idp);
			s.execute();
		}
		s.close();
		s = conn.prepareStatement("DELETE FROM COMMENTAIRE WHERE ID_P = ?"); 
		for (int idp:lproduit){
			s.setInt(1, idp);
			s.execute();
		}
		s.close();
		s = conn.prepareStatement("DELETE FROM PRODUIT WHERE ID_P = ?"); 
		for (int idp:lproduit){
			s.setInt(1, idp);
			s.execute();
		}
		s.close();
	}

	/**
	 * 
	 * @param luser
	 * @throws SQLException
	 */
	public void deleteUsers(List<Integer> luser) throws SQLException {
		PreparedStatement s = conn.prepareStatement("DELETE FROM LINK_CLIENT_CB WHERE ID_C = ? RETURNING ID_CB");
		for (int idc:luser){
			s.setInt(1, idc);
			ResultSet r = s.executeQuery(); 
			if (r.next()){
				PreparedStatement s1 = conn.prepareStatement("DELETE FROM CARTES WHERE ID_CB = ?"); 
				s1.setInt(1, r.getInt(1));
				s1.execute();
				s1.close();
			}
		}
		s.close();
		s = conn.prepareStatement("DELETE FROM LINK_CLIENT_IMAGE WHERE ID_C = ? RETURNING ID_I");
		for(int idc:luser){
			s.setInt(1, idc);
			ResultSet r = s.executeQuery(); 
			if (r.next()){
				PreparedStatement s1 = conn.prepareStatement("DELETE FROM IMAGE WHERE ID_I = ?");
				s1.setInt(1, r.getInt(1));
				s1.execute(); 
				s1.close();
			}
		}
		s.close();
		s = conn.prepareStatement("DELETE FROM COMMENTAIRE WHERE ID_C = ?");
		for (int idc:luser){
			s.setInt(1, idc);
			s.execute(); 
		}
		s.close();
		s = conn.prepareStatement("DELETE FROM ACHAT WHERE ID_C = ? "); 
		for (int idc:luser){
			s.setInt(1, idc);
			s.execute(); 
		}
		s.close(); 
		s = conn.prepareStatement("DELETE FROM CLIENT WHERE ID_C = ?");
		for (int idc:luser){
			s.setInt(1, idc);
			s.execute(); 
		}
		s.close();
	}

	/**
	 * 
	 * @param login
	 * @param encode
	 * @return un Admin 
	 * @throws SQLException
	 */
	public String getAdmin(String login, String encode) throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT LOGIN FROM ADMINISTRATEUR WHERE LOGIN = ? AND HASH_PW = ?"); 
		s.setString(1, login);
		s.setString(2, encode);
		ResultSet r = s.executeQuery();
		String admin = "0";
		if (r.next()){
			admin = r.getString(1);
		}
		return admin;
	}

	/**
	 * 
	 * @return INSERT INTO PRODUIT
	 * @throws SQLException
	 */
	public String exportProduit() throws SQLException {
		List<Produit> lp = getAllProduit();
		String produit = "INSERT INTO PRODUIT (ID_P, NOM, PRIX, VENDEUR) VALUES \n";

		for (Produit p:lp){
			produit += "(" + p.getID_P() + ", '" + p.getNom().replace("\'", "\'\'") + "', "
					+ p.getPrix() + ", '" + p.getVendeur().replace("\'", "\'\'") + "'),\n";
		}
		produit += ";";
		return produit; 
	}

	/**
	 * 
	 * @return INSERT INTO CLIENT
	 * @throws SQLException
	 */
	public String exportClient() throws SQLException {
		List<Client> lc = getAllClient(); 
		String client = "INSERT INTO CLIENT (LOGIN, ID_C, HASH_PW, NOM, PRENOM, ZIP) VALUES \n"; 

		for (Client c:lc){
			client += "('" + c.getLogin().replace("\'", "\'\'") + "' , "
					+ c.getID_C() + ", '"
					+ c.getHash_pw() + "', '" 
					+ c.getNom().replace("\'", "\'\'") + "', '"
					+ c.getPrenom().replace("\'", "\'\'") + "', '"
					+ c.getZip().replace("\'", "\'\'") + "'),\n";
		}
		client += ";";

		return client; 
	}

	/**
	 * 
	 * @return INSERT INTO COMMENTAIRE
	 * @throws SQLException
	 */
	public String exportCommentaire() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_P, ID_C, DATE_COM, NOTE, TEXTE FROM COMMENTAIRE"); 
		ResultSet r = s.executeQuery(); 
		String commentaire = "INSERT INTO COMMENTAIRE (ID_P, ID_C, DATE_COM, NOTE, TEXTE) VALUES \n"; 
		while (r.next()){
			commentaire += "(" + r.getInt(1) + ", " + r.getInt(2) + ", '"
					+ r.getDate(3) + "', " + r.getInt(4) + ", '"
					+ r.getString(5).replace("\'", "\'\'") + "'),\n"; 
		}
		commentaire += ";";
		return commentaire;
	}

	/**
	 * 
	 * @return INSERT INTO ACHAT
	 * @throws SQLException
	 */
	public String exportAchat() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_P, ID_C, DATE_ACHAT FROM ACHAT"); 
		ResultSet r = s.executeQuery(); 
		String ACHAT = "INSERT INTO ACHAT (ID_P, ID_C, DATE_ACHAT) VALUES \n"; 
		while (r.next()){
			ACHAT += "(" + r.getInt(1) + ", " + r.getInt(2) + ", '" + r.getDate(3) + "'),\n";
		}
		ACHAT += ";"; 
		return ACHAT; 
	}

	/**
	 * 
	 * @return INSERT INTO CARTES
	 * @throws SQLException
	 */
	public String exportCartes() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_CB, NUMERO, EXPIRE, CODE FROM CARTES");
		ResultSet r = s.executeQuery(); 
		String cartes = "INSERT INTO CARTES (ID_CB, NUMERO, EXPIRE, CODE) VALUES \n"; 
		while (r.next()){
			cartes += "(" + r.getInt(1) + ", '"
					+ r.getString(2).replace("\'", "\'\'") + "', '"
					+ r.getString(3).replace("\'", "\'\'") + "', '"
					+ r.getString(4).replace("\'", "\'\'") + "'),\n";
		}
		cartes += ";";
		return cartes; 
	}

	/**
	 * 
	 * @return INSERT INTO IMAGE
	 * @throws SQLException
	 */
	public String exportImage() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_I, PATH, TITLE FROM IMAGE");
		ResultSet r = s.executeQuery(); 
		String image = "INSERT INTO IMAGE (ID_I, PATH, TITLE) VALUES \n"; 
		while (r.next()){
			image += "(" + r.getInt(1) + ", '"
					+ r.getString(2).replace("\'", "\'\'") + "', '" 
					+ r.getString(3).replace("\'", "\'\'") + "'),\n";
		}
		image += ";";
		return image;
	}

	/**
	 * 
	 * @return INSERT INTO KEYWORD
	 * @throws SQLException
	 */
	public String exportKeyword() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_KW, NAME FROM KEYWORD");
		ResultSet r = s.executeQuery(); 
		String keyword = "INSERT INTO KEYWORD (ID_KW, NAME) VALUES \n"; 
		while (r.next()){
			keyword += "(" + r.getInt(1) + ", '"
					+ r.getString(2).replace("\'", "\'\'") + "'),\n";
		}
		keyword += ";";
		return keyword;
	}

	/**
	 * 
	 * @return INSERT INTO CATEGORIE
	 * @throws SQLException
	 */
	public String exportCategorie() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_CAT, NAME FROM CATEGORIE");
		ResultSet r = s.executeQuery(); 
		String CATEGORIE = "INSERT INTO CATEGORIE (ID_CAT, NAME) VALUES \n"; 
		while (r.next()){
			CATEGORIE += "(" + r.getInt(1) + ", '"
					+ r.getString(2).replace("\'", "\'\'") + "'),\n";
		}
		CATEGORIE += ";";
		return CATEGORIE;
	}


	/**
	 * 
	 * @return INSERT INTO LINK_PROD_CAT
	 * @throws SQLException
	 */
	public String exportLinkProdCat() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_CAT, ID_P FROM LINK_PROD_CAT");
		ResultSet r = s.executeQuery(); 
		String LINK_PROD_CAT = "INSERT INTO LINK_PROD_CAT (ID_CAT, ID_P) VALUES \n"; 
		while (r.next()){
			LINK_PROD_CAT += "(" + r.getInt(1) + ", "
					+ r.getInt(2) + "),\n";
		}
		LINK_PROD_CAT += ";";
		return LINK_PROD_CAT;
	}

	/**
	 * 
	 * @return INSERT INTO LINK_PROD_KW
	 * @throws SQLException
	 */
	public String exportLinkProdKW() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_KW, ID_P FROM LINK_PROD_KW");
		ResultSet r = s.executeQuery(); 
		String LINK_PROD_KW = "INSERT INTO LINK_PROD_KW (ID_KW, ID_P) VALUES \n"; 
		while (r.next()){
			LINK_PROD_KW += "(" + r.getInt(1) + ", "
					+ r.getInt(2) + "),\n";
		}
		LINK_PROD_KW += ";";
		return LINK_PROD_KW;
	}

	/**
	 * 
	 * @return INSERT INTO LINK_PROD_IMAGE
	 * @throws SQLException
	 */
	public String exportLinkProdImage() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_I, ID_P FROM LINK_PROD_IMAGE");
		ResultSet r = s.executeQuery(); 
		String LINK_PROD_IMAGE = "INSERT INTO LINK_PROD_IMAGE (ID_I, ID_P) VALUES \n"; 
		while (r.next()){
			LINK_PROD_IMAGE += "(" + r.getInt(1) + ", "
					+ r.getInt(2) + "),\n";
		}
		LINK_PROD_IMAGE += ";";
		return LINK_PROD_IMAGE;
	}

	/**
	 * 
	 * @return INSERT INTO LINK_CLIENT_IMAGE
	 * @throws SQLException
	 */
	public String exportLinkClientImage () throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_I, ID_C FROM LINK_CLIENT_IMAGE");
		ResultSet r = s.executeQuery(); 
		String LINK_CLIENT_IMAGE = "INSERT INTO LINK_CLIENT_IMAGE (ID_I, ID_C) VALUES \n"; 
		while (r.next()){
			LINK_CLIENT_IMAGE += "(" + r.getInt(1) + ", "
					+ r.getInt(2) + "),\n";
		}
		LINK_CLIENT_IMAGE += ";";
		return LINK_CLIENT_IMAGE;
	}

	/**
	 * 
	 * @return INSERT INTO LINK_CLIENT_CB
	 * @throws SQLException
	 */
	public String exportLinkClientCB() throws SQLException {
		PreparedStatement s = conn.prepareStatement("SELECT ID_CB, ID_C FROM LINK_CLIENT_CB");
		ResultSet r = s.executeQuery(); 
		String LINK_CLIENT_CB = "INSERT INTO LINK_CLIENT_CB (ID_CB, ID_C) VALUES \n"; 
		while (r.next()){
			LINK_CLIENT_CB += "(" + r.getInt(1) + ", "
					+ r.getInt(2) + "),\n";
		}
		LINK_CLIENT_CB += ";";
		return LINK_CLIENT_CB;

	}

	/**
	 * 
	 * @param categorie
	 * @throws SQLException 
	 */
	public void insertCategorie(List<String> categorie) throws SQLException {
		PreparedStatement s = conn.prepareStatement("INSERT INTO CATEGORIE (ID_CAT, NAME) VALUES (nextval('serial'), ?)"); 
		for (String c : categorie){
			s.setString(1, c);
			s.execute(); 
		}
		s.close();
	}

	/**
	 * 
	 * @param keyword
	 * @throws SQLException
	 */
	public void insertKeyword(List<String> keyword) throws SQLException {
		PreparedStatement s = conn.prepareStatement("INSERT INTO KEYWORD (ID_KW, NAME) VALUES (nextval('serial'), ?)");
		for (String k : keyword){
			s.setString(1, k);
			s.execute(); 
		}
		s.close();

	}

	/**
	 * 
	 * @param iD_C
	 * @param panier
	 * @return
	 * @throws SQLException
	 */
	public Client insertAchat(int iD_C, ArrayList<ShoppingCartItem> panier) throws SQLException {
		PreparedStatement s = conn.prepareStatement("INSERT INTO ACHAT (ID_C, ID_P, QUANTITE, DATE_ACHAT) "
				+ "VALUES (? ,? ,? , current_timestamp)"); 
		s.setInt(1, iD_C);
		for (ShoppingCartItem achat : panier){
			s.setInt(2, achat.getID_P().getID_P());
			s.setInt(3, achat.getQuantite());
			s.execute(); 
		}
		s.close();

		return getClient(iD_C);
	}

	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Float getCA () throws SQLException{
		PreparedStatement s = conn.prepareStatement("SELECT SUM (ACHAT.QUANTITE * PRODUIT.PRIX) AS SOMME, "
				+ " COUNT (*) FROM PUBLIC.ACHAT, PUBLIC.PRODUIT WHERE ACHAT.ID_P = PRODUIT.ID_P");
		ResultSet r = s.executeQuery(); 
		if (r.next()) {
			return r.getFloat(1);
		} else {
			throw new SQLException();
		}

	}

	/**
	 * Renvoie les produits similaires 
	 * @param p
	 * @return
	 * @throws SQLException
	 */
	public List<Produit> getProduitsSimilaires (Produit p) throws SQLException {

		List<Categorie> cat = p.getCategorie(); 
		List<Keyword> kw = p.getKeyword(); 

		List<String> intCat = new LinkedList<String>(); 
		String categories = "(";
		for ( Categorie c : cat){
			intCat.add(Integer.toString(c.getID_CAT()));
		}
		categories += String.join(",", intCat); 
		categories += ")"; 




		List<String> intKw = new LinkedList<String>(); 
		String keywords = "(";
		for (Keyword k : kw) {
			intKw.add(Integer.toString(k.getID_KW()));
		}
		keywords += String.join(",", intKw); 
		keywords += ")"; 

		PreparedStatement s = conn.prepareStatement("SELECT "
				+ "produit.id_p FROM "
				+ "public.categorie, "
				+ "public.keyword, "
				+ "public.produit, "
				+ "public.link_prod_cat, "
				+ "public.link_prod_kw "
				+ "WHERE link_prod_cat.id_cat = categorie.id_cat AND "
				+ "link_prod_cat.id_p = produit.id_p AND "
				+ "link_prod_kw.id_kw = keyword.id_kw AND "
				+ "link_prod_kw.id_p = produit.id_p AND "
				+ "(keyword.id_kw IN "+ keywords + " OR categorie.id_cat IN "+ categories + " ) "
				+ "GROUP BY produit.id_p ");

		List<Produit> resultat = new LinkedList<Produit>(); 
		ResultSet r = s.executeQuery(); 
		while (r.next()){
			resultat.add(getProduit(r.getInt(1)));
		}

		return resultat;

	}

	public Object updateCommentaire(int idp, int idc, String text) throws SQLException {
		PreparedStatement s = conn.prepareStatement("UPDATE COMMENTAIRE SET TEXTE = ? WHERE ID_P = ? AND ID_C = ? ");
		s.setString(1, text);
		s.setInt(2, idp);
		s.setInt(3, idc); 
		s.execute(); 

		return getProduit(idp);
	}

}
