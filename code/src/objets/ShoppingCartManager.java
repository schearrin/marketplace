package objets;

import java.util.ArrayList;

/**
 * 2016-12-28
 * Tuto http://wwwdi.supelec.fr/hardebolle/tutos/TutorielJavaEE/JEE_31-panier.php
 * Techniquement, j'ai pas tout suivi
 * @author Arutha
 *
 */

public class ShoppingCartManager {

	private ArrayList<ShoppingCartItem> panier;
	private int cardinal; 
	private float valeur; 
	

	public ShoppingCartManager() {
		this.panier = new ArrayList<ShoppingCartItem>(); 
	}
	
	/**
	 * @return the panier
	 */
	public ArrayList<ShoppingCartItem> getPanier() {
		return panier;
	}

	/**
	 * @param panier the panier to set
	 */
	public void setPanier(ArrayList<ShoppingCartItem> panier) {
		this.panier = panier;
	} 
	/**
	 * Pour ajouter un objet au panier 
	 * @param produit
	 */
	public void addProduit (ShoppingCartItem produit ){
		this.panier.add(produit);
		this.cardinal = getCardinal() + produit.getQuantite();  
		this.valeur = getValeur() + produit.getPrix(); 
	}

	/**
	 * @return the cardinal
	 */
	public int getCardinal() {
		return cardinal;
	}

	/**
	 * @param cardinal the cardinal to set
	 */
	public void setCardinal(int cardinal) {
		this.cardinal = cardinal;
	}

	/**
	 * @return the valeur
	 */
	public float getValeur() {
		return valeur;
	}

	/**
	 * @param valeur the valeur to set
	 */
	public void setValeur(float valeur) {
		this.valeur = valeur;
	}
	
	
}
