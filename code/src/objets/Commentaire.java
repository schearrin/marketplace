package objets;

/**
 * @author Arutha
 *
 */
public class Commentaire {
	private String username;
	private String text;
	private int note; 
	private String date;
	private int idc; 
	
	public Commentaire (String u, String t,int i, String d, int idc){
		setUsername(u);
		setText(t);
		setNote(i);
		setDate(d);
		setIdc(idc);
	}
	


	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the note
	 */
	public int getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(int note) {
		this.note = note;
	}



	/**
	 * @return the idc
	 */
	public int getIdc() {
		return idc;
	}



	/**
	 * @param idc the idc to set
	 */
	public void setIdc(int idc) {
		this.idc = idc;
	}

}