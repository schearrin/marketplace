package objets;

/**
 * 
 * @author Arutha
 *	Classe Categorie
 */
public class Categorie {
	private int ID_CAT; 
	private String name;
	
	public Categorie (int i, String name){
		setID_CAT(i); 
		setName(name); 
	}
	
	@Override 
	public boolean equals (Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (!(o instanceof Categorie))return false;
		Categorie v = (Categorie) o;
		return (this.ID_CAT == v.ID_CAT);
	}
	
	/**
	 * @return the iD_CAT
	 */
	public int getID_CAT() {
		return ID_CAT;
	}
	/**
	 * @param iD_CAT the iD_CAT to set
	 */
	public void setID_CAT(int iD_CAT) {
		ID_CAT = iD_CAT;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	} 
	
}
