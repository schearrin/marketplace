package objets;

/**
 * 2016-12-17
 * @author Sophie
 * Classe Administrateur
 */

public class Administrateur {
	private String login; 
	private int ID_ADMIN; 
	private String hash_pw; 

	/**
	 * 
	 * @param login
	 * @param hash
	 */
	public Administrateur(String login, String hash){
		setLogin(login);
		setHash_pw(hash);
	}
	
	
	/**
	 * @return the iD_ADMIN
	 */
	public int getID_ADMIN() {
		return ID_ADMIN;
	}
	/**
	 * @param iD_ADMIN the iD_ADMIN to set
	 */
	public void setID_ADMIN(int iD_ADMIN) {
		ID_ADMIN = iD_ADMIN;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the hash_pw
	 */
	public String getHash_pw() {
		return hash_pw;
	}
	/**
	 * @param hash_pw the hash_pw to set
	 */
	public void setHash_pw(String hash_pw) {
		this.hash_pw = hash_pw;
	}
}
