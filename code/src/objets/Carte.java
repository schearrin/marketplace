package objets;

public class Carte {
	private int ID_CB; 
	private String num; // numero � 16 chiffres --Oui mais mettre une String et un Varchar dans la base
	private String date; // date au format aaaa-mm, on peut m�me s'amuser � faire un select, avec des choix de 2016 � 2030 et de 1 � 12 
	private String code; // code � trois chiffres
	
	public Carte (int i,String string, String d, String c){
		setID_CB(i);
		setNum(string);
		setDate(d);
		setCode(c);
	}

	/**
	 * @return the num
	 */
	public String getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(String num) {
		this.num = num;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the iD_CB
	 */
	public int getID_CB() {
		return ID_CB;
	}

	/**
	 * @param iD_CB the iD_CB to set
	 */
	public void setID_CB(int iD_CB) {
		ID_CB = iD_CB;
	}
}
