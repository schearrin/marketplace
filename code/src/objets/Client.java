package objets;

import java.util.List;



/**
 * 2016-12-17
 * @author Arutha
 * Classe Client 
 */
public class Client {
	private String login; 
	private int ID_C; 
	private String hash_pw; 
	private String nom; 
	private String prenom; 
	private String zip; 
	
	private List<Image> photo; 
	private List<Carte> carte;
	private List<Achat> achat; 
	
	/**
	 * 
	 * @param login
	 * @param idc
	 * @param hash
	 * @param nom
	 * @param prenom
	 * @param zip
	 * @param li
	 * @param lc
	 * @param la
	 */
	public Client (String login, int idc, String hash, String nom, String prenom, String zip, List<Image> li, List<Carte> lc, List<Achat> la) {
		setLogin(login);
		setID_C(idc);
		setHash_pw(hash);
		setNom(nom);
		setPrenom(prenom);
		setZip(zip); 
		
		setPhoto(li);
		setCarte(lc);
		setAchat(la);
	}
	
	@Override 
	public boolean equals (Object o) {
		if (o == null) return false;
		if (o == this) return true;
		if (!(o instanceof Client))return false;
		Client v = (Client) o;
		return (this.ID_C == v.ID_C);
	}
	
	
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the iD_C
	 */
	public int getID_C() {
		return ID_C;
	}
	/**
	 * @param iD_C the iD_C to set
	 */
	public void setID_C(int iD_C) {
		ID_C = iD_C;
	}
	/**
	 * @return the hash_pw
	 */
	public String getHash_pw() {
		return hash_pw;
	}
	/**
	 * @param hash_pw the hash_pw to set
	 */
	public void setHash_pw(String hash_pw) {
		this.hash_pw = hash_pw;
	}
	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the photo
	 */
	public List<Image> getPhoto() {
		return photo;
	}

	/**
	 * @param photo the photo to set
	 */
	public void setPhoto(List<Image> photo) {
		this.photo = photo;
	}

	/**
	 * @return the carte
	 */
	public List<Carte> getCarte() {
		return carte;
	}

	/**
	 * @param carte the carte to set
	 */
	public void setCarte(List<Carte> carte) {
		this.carte = carte;
	}

	/**
	 * @return the achat
	 */
	public List<Achat> getAchat() {
		return achat;
	}

	/**
	 * @param achat the achat to set
	 */
	public void setAchat(List<Achat> achat) {
		this.achat = achat;
	}
	
	
	
}
