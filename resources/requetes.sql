SELECT  categorie.id_cat,categorie.name FROM   public.categorie,   public.produit,   public.link_prod_cat WHERE   categorie.id_cat = link_prod_cat.id_cat AND   link_prod_cat.id_p = produit.id_p AND   produit.id_p = ?;


SELECT 
  keyword.id_kw, 
  keyword.name
FROM 
  public.keyword, 
  public.produit, 
  public.link_prod_kw
  
WHERE 
  keyword.id_kw = link_prod_kw.id_kw AND
  link_prod_kw.id_p = produit.id_p AND
  produit.id_p = ?;
  
SELECT keyword.id_kw, keyword.name FROM public.keyword, public.produit, public.link_prod_kw WHERE keyword.id_kw = link_prod_kw.id_kw AND link_prod_kw.id_p = produit.id_p AND produit.id_p = ?; 


SELECT 
  client.login, 
  commentaire.texte,
  commentaire.date_com, 
  commentaire.note, 
  client.id_c
FROM 
  public.commentaire, 
  public.client, 
  public.produit
WHERE 
  commentaire.id_c = client.id_c AND
  produit.id_p = commentaire.id_p;
  
SELECT client.login, commentaire.texte, commentaire.date_com, commentaire.note, client.id_c FROM public.commentaire, public.client, public.produit WHERE commentaire.id_c = client.id_c AND produit.id_p = commentaire.id_p AND produit.id_p = ?;





SELECT 
  image.id_i, 
  image.path
FROM 
  public.produit,
  public.image, 
  public.link_prod_image
WHERE 
  image.id_i = link_prod_image.id_i AND
  link_prod_image.id_p = produit.id_p AND 
  produit.id_p = ? ;

SELECT image.path, image.id_i FROM public.produit, public.image, public.link_prod_image WHERE image.id_i = link_prod_image.id_i AND link_prod_image.id_p = produit.id_p AND produit.id_p = ? 










SELECT 
  produit.nom, 
  produit.prix, 
  produit.vendeur
FROM 
  public.produit
WHERE 
  produit.id_p = ?;

SELECT produit.nom, produit.prix, produit.vendeur FROM public.produit WHERE produit.id_p = ?; 




SELECT 
  cartes.numero, 
  cartes.expire, 
  cartes.code
FROM 
  public.client, 
  public.link_client_cb, 
  public.cartes
WHERE 
  link_client_cb.id_c = client.id_c AND
  cartes.id_cb = link_client_cb.id_cb AND 
  client.id_c = ?;

SELECT cartes.numero, cartes.expire, cartes.code FROM public.client, public.link_client_cb, public.cartes WHERE link_client_cb.id_c = client.id_c AND cartes.id_cb = link_client_cb.id_cb AND client.id_c = ?; 









SELECT 
  image.id_i,
  image.path, 
  link_client_image.title
FROM 
  public.image, 
  public.client, 
  public.link_client_image
WHERE 
  client.id_c = link_client_image.id_c AND
  link_client_image.id_i = image.id_i AND 
  client.id_c = ?;

SELECT image.id_i, image.path, link_client_image.title FROM public.image, public.client, public.link_client_image WHERE client.id_c = link_client_image.id_c AND link_client_image.id_i = image.id_i AND client.id_c = ? 

SELECT 
  achat.date_achat, 
  achat.id_p
FROM 
  public.client, 
  public.achat
WHERE 
  client.id_c = achat.id_c AND 
  client.id_c = ?;
  
SELECT achat.date_achat, achat.id_p FROM public.client, public.achat WHERE client.id_c = achat.id_c AND client.id_c = ?;  

SELECT 
  sum (achat.quantite * produit.prix) as Somme,  
  count (*)
FROM 
  public.achat, 
  public.produit
WHERE 
  achat.id_p = produit.id_p;
  
  
SELECT SUM (ACHAT.QUANTITE * PRODUIT.PRIX) AS SOMME,  COUNT (*) FROM PUBLIC.ACHAT, PUBLIC.PRODUIT WHERE ACHAT.ID_P = PRODUIT.ID_P  


SELECT 
  avg(commentaire.note) AS MOYENNE
FROM 
  public.commentaire, 
  public.produit
WHERE 
  produit.id_p = commentaire.id_p AND
  produit.id_p = ?;
  
SELECT avg(commentaire.note) AS MOYENNE FROM public.commentaire, public.produit WHERE produit.id_p = commentaire.id_p AND produit.id_p = ?;  




SELECT 
  produit.nom, 
  produit.id_p
FROM 
  public.categorie, 
  public.keyword, 
  public.produit, 
  public.link_prod_cat, 
  public.link_prod_kw
WHERE 
  link_prod_cat.id_cat = categorie.id_cat AND
  link_prod_cat.id_p = produit.id_p AND
  link_prod_kw.id_kw = keyword.id_kw AND
  link_prod_kw.id_p = produit.id_p AND 
  (keyword.id_kw = ? OR 
  categorie.id_cat = ?) 
GROUP BY produit.id_p


SELECT produit.nom, produit.id_p FROM public.categorie, public.keyword, public.produit, public.link_prod_cat, public.link_prod_kw WHERE link_prod_cat.id_cat = categorie.id_cat AND link_prod_cat.id_p = produit.id_p AND link_prod_kw.id_kw = keyword.id_kw AND link_prod_kw.id_p = produit.id_p AND (keyword.id_kw = ? OR categorie.id_cat = ?) GROUP BY produit.id_p 


SELECT 
  produit.nom, 
  produit.id_p
FROM 
  public.categorie, 
  public.keyword, 
  public.produit, 
  public.link_prod_cat, 
  public.link_prod_kw
WHERE 
  link_prod_cat.id_cat = categorie.id_cat AND
  link_prod_cat.id_p = produit.id_p AND
  link_prod_kw.id_kw = keyword.id_kw AND
  link_prod_kw.id_p = produit.id_p AND 
  (keyword.id_kw IN (3,2) OR 
  categorie.id_cat IN (1,2) ) 
GROUP BY produit.id_p


PreparedStatement pstmt = 
                conn.prepareStatement("select * from employee where id in (?)");
Array array = conn.createArrayOf("VARCHAR", new Object[]{"1", "2","3"});
pstmt.setArray(1, array);
ResultSet rs = pstmt.executeQuery();


String parameters = StringUtils.join(arraylistParameter.iterator(),",");  
 PreparedStatement pstmt =conn.prepareStatement("select * from employee where id in (?)");    
 pstmt.setString(1, parameters );  
 ResultSet rs = pstmt.executeQuery();


SELECT produit.id_p 
FROM public.categorie, public.keyword, public.produit, public.link_prod_cat, public.link_prod_kw 
WHERE link_prod_cat.id_cat = categorie.id_cat AND link_prod_cat.id_p = produit.id_p AND link_prod_kw.id_kw = keyword.id_kw AND 
link_prod_kw.id_p = produit.id_p AND 
(keyword.id_kw IN '{"objets.Keyword@4613e9be"}' OR categorie.id_cat IN '{"objets.Categorie@4b195601"}') 
GROUP BY produit.id_p 

String parameters = StringUtils.join(arraylistParameter.iterator(),",");  
PreparedStatement pstmt =conn.prepareStatement("select * from employee where id in ("+ parameters +")");


SELECT produit.id_p 
FROM public.categorie, public.keyword, public.produit, public.link_prod_cat, public.link_prod_kw 
WHERE link_prod_cat.id_cat = categorie.id_cat AND 
link_prod_cat.id_p = produit.id_p AND 
link_prod_kw.id_kw = keyword.id_kw AND 
link_prod_kw.id_p = produit.id_p AND 
(keyword.id_kw IN '{"2"}' OR categorie.id_cat IN '{"1"}') 
GROUP BY produit.id_p 

