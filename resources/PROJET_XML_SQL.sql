﻿------------------------------------------------------------
--        Script Postgre 
------------------------------------------------------------
DROP TABLE ACHAT; 
DROP TABLE LINK_CLIENT_CB;
DROP TABLE LINK_CLIENT_IMAGE;
DROP TABLE LINK_PROD_CAT;
DROP TABLE LINK_PROD_IMAGE;
DROP TABLE LINK_PROD_KW;
DROP TABLE CARTES; 
DROP TABLE CATEGORIE; 
DROP TABLE COMMENTAIRE;
DROP TABLE IMAGE;
DROP TABLE KEYWORD;
DROP TABLE CLIENT; 
DROP TABLE PRODUIT; 


------------------------------------------------------------
-- Table: Produit
------------------------------------------------------------
CREATE TABLE public.Produit(
	ID_P    INT  NOT NULL ,
	NOM     VARCHAR (255)  ,
	PRIX    FLOAT   ,
	VENDEUR VARCHAR (255)  ,
	CONSTRAINT prk_constraint_Produit PRIMARY KEY (ID_P)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Categorie
------------------------------------------------------------
CREATE TABLE public.Categorie(
	ID_CAT INT  NOT NULL ,
	NAME   VARCHAR (255)  ,
	CONSTRAINT prk_constraint_Categorie PRIMARY KEY (ID_CAT)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Keyword
------------------------------------------------------------
CREATE TABLE public.Keyword(
	ID_KW INT  NOT NULL ,
	NAME  VARCHAR (255)  ,
	CONSTRAINT prk_constraint_Keyword PRIMARY KEY (ID_KW)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Image
------------------------------------------------------------
CREATE TABLE public.Image(
	ID_I INT  NOT NULL ,
  TITLE VARCHAR (255)  ,
	PATH VARCHAR (255)  ,
	CONSTRAINT prk_constraint_Image PRIMARY KEY (ID_I)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Client
------------------------------------------------------------
CREATE TABLE public.Client(
	LOGIN   VARCHAR (255) NOT NULL UNIQUE,
	ID_C    INT  NOT NULL ,
	HASH_PW VARCHAR (255)  ,
	NOM     VARCHAR (255)  ,
	PRENOM  VARCHAR (255)  ,
	ZIP     VARCHAR (5)   ,
	CONSTRAINT prk_constraint_Client PRIMARY KEY (ID_C)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: ADMINISTRATEUR jbs argonath 
------------------------------------------------------------
CREATE TABLE public.ADMINISTRATEUR(
  ID_A    INT  NOT NULL ,
	LOGIN   VARCHAR (255) NOT NULL UNIQUE,
	HASH_PW VARCHAR (255) ,
	CONSTRAINT prk_constraint_Admin PRIMARY KEY (ID_A)
)WITHOUT OIDS;

INSERT INTO ADMINISTRATEUR (ID_A, LOGIN, HASH_PW) VALUES 
(1, 'jbs', '52d0f099b8599248ec0b251a4f39ff02');

------------------------------------------------------------
-- Table: Cartes
------------------------------------------------------------
CREATE TABLE public.Cartes(
	ID_CB  INT  NOT NULL ,
	NUMERO VARCHAR (16)  ,
	EXPIRE VARCHAR (16)  ,
	CODE   VARCHAR (3)  ,
	CONSTRAINT prk_constraint_Cartes PRIMARY KEY (ID_CB)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Link_Prod_Cat
------------------------------------------------------------
CREATE TABLE public.Link_Prod_Cat(
	ID_CAT INT  NOT NULL ,
	ID_P   INT  NOT NULL ,
	CONSTRAINT prk_constraint_Link_Prod_Cat PRIMARY KEY (ID_CAT,ID_P)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Link_Prod_KW
------------------------------------------------------------
CREATE TABLE public.Link_Prod_KW(
	ID_KW INT  NOT NULL ,
	ID_P  INT  NOT NULL ,
	CONSTRAINT prk_constraint_Link_Prod_KW PRIMARY KEY (ID_KW,ID_P)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Link_Prod_Image
------------------------------------------------------------
CREATE TABLE public.Link_Prod_Image(
	ID_I INT  NOT NULL ,
	ID_P INT  NOT NULL ,
	CONSTRAINT prk_constraint_Link_Prod_Image PRIMARY KEY (ID_I,ID_P)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Achat
------------------------------------------------------------
CREATE TABLE public.Achat(
	DATE_ACHAT DATE   ,
  QUANTITE  INT , 
	ID_P       INT  NOT NULL ,
	ID_C       INT  NOT NULL ,
	CONSTRAINT prk_constraint_Achat PRIMARY KEY (ID_P,ID_C)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Commentaire
------------------------------------------------------------
CREATE TABLE public.Commentaire(
	DATE_COM DATE   ,
	NOTE     INT   ,
	TEXTE    VARCHAR (255)  ,
	ID_P     INT  NOT NULL ,
	ID_C     INT  NOT NULL ,
	CONSTRAINT prk_constraint_Commentaire PRIMARY KEY (ID_P,ID_C)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Link_Client_Image
------------------------------------------------------------
CREATE TABLE public.Link_Client_Image(
	ID_I  INT  NOT NULL ,
	ID_C  INT  NOT NULL ,
	CONSTRAINT prk_constraint_Link_Client_Image PRIMARY KEY (ID_I,ID_C)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: Link_Client_CB
------------------------------------------------------------
CREATE TABLE public.Link_Client_CB(
	ID_C  INT  NOT NULL ,
	ID_CB INT  NOT NULL ,
	CONSTRAINT prk_constraint_Link_Client_CB PRIMARY KEY (ID_C,ID_CB)
)WITHOUT OIDS;



ALTER TABLE public.Link_Prod_Cat ADD CONSTRAINT FK_Link_Prod_Cat_ID_CAT FOREIGN KEY (ID_CAT) REFERENCES public.Categorie(ID_CAT);
ALTER TABLE public.Link_Prod_Cat ADD CONSTRAINT FK_Link_Prod_Cat_ID_P FOREIGN KEY (ID_P) REFERENCES public.Produit(ID_P);
ALTER TABLE public.Link_Prod_KW ADD CONSTRAINT FK_Link_Prod_KW_ID_KW FOREIGN KEY (ID_KW) REFERENCES public.Keyword(ID_KW);
ALTER TABLE public.Link_Prod_KW ADD CONSTRAINT FK_Link_Prod_KW_ID_P FOREIGN KEY (ID_P) REFERENCES public.Produit(ID_P);
ALTER TABLE public.Link_Prod_Image ADD CONSTRAINT FK_Link_Prod_Image_ID_I FOREIGN KEY (ID_I) REFERENCES public.Image(ID_I);
ALTER TABLE public.Link_Prod_Image ADD CONSTRAINT FK_Link_Prod_Image_ID_P FOREIGN KEY (ID_P) REFERENCES public.Produit(ID_P);
ALTER TABLE public.Achat ADD CONSTRAINT FK_Achat_ID_P FOREIGN KEY (ID_P) REFERENCES public.Produit(ID_P);
ALTER TABLE public.Achat ADD CONSTRAINT FK_Achat_ID_C FOREIGN KEY (ID_C) REFERENCES public.Client(ID_C);
ALTER TABLE public.Commentaire ADD CONSTRAINT FK_Commentaire_ID_P FOREIGN KEY (ID_P) REFERENCES public.Produit(ID_P);
ALTER TABLE public.Commentaire ADD CONSTRAINT FK_Commentaire_ID_C FOREIGN KEY (ID_C) REFERENCES public.Client(ID_C);
ALTER TABLE public.Link_Client_Image ADD CONSTRAINT FK_Link_Client_Image_ID_I FOREIGN KEY (ID_I) REFERENCES public.Image(ID_I);
ALTER TABLE public.Link_Client_Image ADD CONSTRAINT FK_Link_Client_Image_ID_C FOREIGN KEY (ID_C) REFERENCES public.Client(ID_C);
ALTER TABLE public.Link_Client_CB ADD CONSTRAINT FK_Link_Client_CB_ID_C FOREIGN KEY (ID_C) REFERENCES public.Client(ID_C);
ALTER TABLE public.Link_Client_CB ADD CONSTRAINT FK_Link_Client_CB_ID_CB FOREIGN KEY (ID_CB) REFERENCES public.Cartes(ID_CB);
